package com.mlpg.examples.graph;

import org.graphstream.algorithm.generator.Generator;
import org.graphstream.stream.rmi.RMISink;

import java.rmi.RemoteException;

public final class DRmiMachineSource
{
	private DRmiMachineSource()
	{
	}

	public static void main(String[] args)
	{
		try
		{
			java.rmi.registry.LocateRegistry.createRegistry(1099);
		}
		catch (Exception e)
		{
			// e.printStackTrace();
		}

		try
		{
			RMISink sink = new RMISink("sink_test");
			sink.register("//localhost/rmi_test");

			Generator gen = new DNodeAndEdgeGenerator();
			gen.addSink(sink);

			gen.begin();
			boolean ne = false;

			do
			{
				try
				{
					ne = gen.nextEvents();
				}
				catch (Exception e)
				{
					// target not available(?)
				}

				final int sleepTime = 1000;
				Thread.sleep(sleepTime);
			} while (ne);

			gen.end();
			gen.removeSink(sink);
		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
