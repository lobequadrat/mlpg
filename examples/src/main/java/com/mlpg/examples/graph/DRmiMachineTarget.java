package com.mlpg.examples.graph;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.rmi.RMISource;

import java.rmi.RemoteException;

public final class DRmiMachineTarget
{
	private DRmiMachineTarget()
	{
	}

	public static void main(String[] args)
	{
		try
		{
			java.rmi.registry.LocateRegistry.createRegistry(1099);
		}
		catch (Exception e)
		{
			// e.printStackTrace();
		}

		System.setProperty("org.graphstream.ui", "javafx");
		Graph graph = new SingleGraph("sg");

		RMISource source = null;
		try
		{
			source = new RMISource("rmi_test");
			source.addSink(graph);
		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}

		graph.display();
	}
}
