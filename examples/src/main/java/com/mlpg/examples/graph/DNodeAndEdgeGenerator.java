package com.mlpg.examples.graph;

import org.graphstream.algorithm.generator.Generator;
import org.graphstream.stream.SourceBase;

public class DNodeAndEdgeGenerator extends SourceBase implements Generator
{
	private static final int X_FIRST_LINE = 100;
	private static final int X_LAST_LINE = 2000;
	private static final int X_INC = 100;


	private static final int Y_FIRST_LINE = 2000;
	private static final int Y_LAST_LINE = 0;
	private static final int Y_INC = -100;

	private int x = X_FIRST_LINE;
	private int y = Y_FIRST_LINE;

	private static int nodeID = 1;

	public void begin()
	{
		sendGraphCleared(sourceId);
		drawLine(X_FIRST_LINE);
	}

	private void drawLine(int xParameter)
	{
		for (y = Y_FIRST_LINE; y != Y_LAST_LINE; y += Y_INC)
		{
			String sNodeID = String.valueOf(nodeID++);
			sendNodeAdded(sourceId, sNodeID);
			sendNodeAttributeChanged(sourceId, sNodeID, "ui.style", null, "size: 20px, 20px; stroke-mode: plain;"
					+ "stroke-color: black; fill-color: rgb(255,0,0);");

			sendNodeAttributeChanged(sourceId, sNodeID, "x", null, xParameter);
			sendNodeAttributeChanged(sourceId, sNodeID, "y", null, y);
			sendNodeAttributeChanged(sourceId, sNodeID, "ui.label", null, sNodeID);
		}
	}

	public boolean nextEvents()
	{
		if (x < X_LAST_LINE)
			return false;

		x += X_INC;
		drawLine(x);
		return true;
	}

	public void end()
	{
		// presumably cleanup
	}
}
