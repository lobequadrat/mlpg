package com.mlpg.examples.graph;

import org.graphstream.algorithm.generator.Generator;
import org.graphstream.stream.SourceBase;

public class BNodeAndEdgeGenerator extends SourceBase implements Generator
{
	private int currentIndex;
	private int edgeId;

	public void begin()
	{
		//sendGraphCleared(sourceId);
		addNode();
	}

	public boolean nextEvents()
	{
		addNode();
		return true;
	}

	public void end()
	{
		// Nothing to do
	}

	protected void addNode()
	{
		sendNodeAdded(sourceId, Integer.toString(currentIndex));

		for (int i = 0; i < currentIndex; i++)
			sendEdgeAdded(sourceId, Integer.toString(edgeId++),
					Integer.toString(i), Integer.toString(currentIndex), false);

		currentIndex++;
	}
}
