package com.mlpg.examples.graph;

public final class GraphConstants
{

	private GraphConstants()
	{
	}

	/** CSS_GRAPH_PADDING. */
	public static final String CSS_GRAPH_PADDING = "graph {padding: 100px;}";

}
