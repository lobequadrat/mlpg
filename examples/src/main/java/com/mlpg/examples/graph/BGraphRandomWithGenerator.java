package com.mlpg.examples.graph;

import org.graphstream.algorithm.generator.Generator;
import org.graphstream.graph.implementations.MultiGraph;


public final class BGraphRandomWithGenerator
{
	private BGraphRandomWithGenerator()
	{
	}

	/** NODES. */
	public static final int NODES = 100;

	/** EDGES. */
	public static final int EDGES = 100;

	public static void main(String[] args)
	{
		System.setProperty("org.graphstream.ui", "javafx");
		MultiGraph graph = new MultiGraph("mg");

		// DorogovtsevMendesGenerator gen = new DorogovtsevMendesGenerator();

		graph.setAttribute("ui.antialias");
		graph.setAttribute("ui.quality");
		graph.setAttribute("ui.stylesheet", GraphConstants.CSS_GRAPH_PADDING);

		Generator gen = new BNodeAndEdgeGenerator();

		gen.addSink(graph);
		gen.begin();
		for (int i = 0; i < 50; i++)
			gen.nextEvents();
		gen.end();
		gen.removeSink(graph);

		graph.display();
	}
}
