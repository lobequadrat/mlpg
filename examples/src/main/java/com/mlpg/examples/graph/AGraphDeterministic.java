package com.mlpg.examples.graph;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;


public final class AGraphDeterministic
{
	private AGraphDeterministic()
	{
	}

	public static void main(String[] args)
	{
		System.setProperty("org.graphstream.ui", "javafx");

		Graph graph = new SingleGraph("sg");
		graph.setAttribute("ui.antialias");
		graph.setAttribute("ui.quality");


		final int xA = 100;
		final int yA = 100;
		Node n = graph.addNode("A");
		n.setAttribute("x", xA);
		n.setAttribute("y", yA);
		n.setAttribute("ui.style",
				"size: 20px, 20px; shape: box; stroke-mode: plain;"
						+ "stroke-color: black; fill-color: rgb(255,0,0);");
		n.setAttribute("ui.label", "A");

		final int xB = 100;
		final int yB = 100;
		n = graph.addNode("B");
		n.setAttribute("x", xB);
		n.setAttribute("y", yB);
		n.setAttribute("ui.style", "fill-color: rgb(0,255,0);");
		n.setAttribute("ui.label", "B");

		final int xC = 100;
		final int yC = 100;
		n = graph.addNode("C");
		n.setAttribute("x", xC);
		n.setAttribute("y", yC);
		n.setAttribute("ui.style", "fill-color: rgb(0,0,255);");
		n.setAttribute("ui.label", "C");

		Edge e = graph.addEdge("AB", "A", "B");
		e.setAttribute("ui.label", "AB");

		e = graph.addEdge("BC", "B", "C");
		e.setAttribute("ui.label", "BC");

		e = graph.addEdge("CA", "C", "A");
		e.setAttribute("ui.label", "CA");

		graph.display();
	}
}
