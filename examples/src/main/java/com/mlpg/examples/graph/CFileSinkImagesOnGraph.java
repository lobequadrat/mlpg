package com.mlpg.examples.graph;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSinkImages;
import org.graphstream.stream.file.images.Resolutions;

public final class CFileSinkImagesOnGraph
{
	private CFileSinkImagesOnGraph()
	{

	}

	public static void main(String[] args)
	{
		System.setProperty("org.graphstream.ui", "javafx");

		Graph graph = new SingleGraph("sg");

		// test to create images
		FileSinkImages fsi = FileSinkImages.createDefault();
		fsi.setOutputPolicy(FileSinkImages.OutputPolicy.BY_STEP);
		fsi.setResolution(Resolutions.VGA);
		fsi.setOutputType(FileSinkImages.OutputType.PNG);

		graph.addSink(fsi);

		try
		{
			fsi.begin("picture_");

			final int x1 = 100;
			final int y1 = 100;
			Node n = graph.addNode("A");
			n.setAttribute("x", x1);
			n.setAttribute("y", y1);
			n.setAttribute("ui.style",
					"size: 20px, 20px; shape: box; fill-color: green; stroke-mode: plain;"
							+ "stroke-color: black; fill-color: rgb(255,0,0);");
			n.setAttribute("ui.label", "A");

			final int timeId1 = 1;
			final int step1 = 1;
			final int x2 = 200;
			final int y2 = 200;
			fsi.stepBegins("sourceID", timeId1, step1);
			n = graph.addNode("B");
			n.setAttribute("x", x2);
			n.setAttribute("y", y2);
			n.setAttribute("ui.style", "fill-color: rgb(0,255,0);");
			n.setAttribute("ui.label", "B");

			final int timeId2 = 2;
			final int step2 = 2;
			final int x3 = 300;
			final int y3 = 300;
			fsi.stepBegins("sourceID", timeId2, step2);
			n = graph.addNode("C");
			n.setAttribute("x", x3);
			n.setAttribute("y", y3);
			n.setAttribute("ui.style", "fill-color: rgb(0,0,255);");
			n.setAttribute("ui.label", "C");

			final int timeId3 = 3;
			final int step3 = 4;
			fsi.stepBegins("sourceID", timeId3, step3);
			Edge e = graph.addEdge("AB", "A", "B");
			e.setAttribute("ui.label", "AB");
			e = graph.addEdge("BC", "B", "C");
			e.setAttribute("ui.label", "BC");

			final int timeId4 = 5;
			final int step4 = 5;
			fsi.stepBegins("sourceID", timeId4, step4);
			e = graph.addEdge("CA", "C", "A");
			e.setAttribute("ui.label", "CA");

			fsi.end();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			graph.removeSink(fsi);
		}

		graph.display();
	}
}
