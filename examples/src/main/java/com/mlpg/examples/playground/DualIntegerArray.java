package com.mlpg.examples.playground;

import java.util.Arrays;

public class DualIntegerArray
{
	private boolean[] booleanArray;


	public DualIntegerArray(int number)
	{
		int absNumber = Math.abs(number);

		// determine array size
		int arraySize = 2;                  // first one stores sign
		int powerOfTwo = 1;
		while (powerOfTwo * 2 <= absNumber)
		{
			arraySize++;
			powerOfTwo *= 2;
		}

		this.booleanArray = new boolean[arraySize];
		this.booleanArray[0] = number > 0;         // true = >0

		int arrayPosition = arraySize - 1;
		while (powerOfTwo > 0)
		{
			if (absNumber / powerOfTwo >= 1)
			{
				this.booleanArray[arrayPosition] = true;
				absNumber -= powerOfTwo;
			}
			else
				this.booleanArray[arrayPosition] = false;

			powerOfTwo /= 2;
			arrayPosition--;
		}
	}


	int[] toIntArray()
	{
		int[] integerArray = new int[booleanArray.length];

		for (int i = 0; i < booleanArray.length; i++)
			integerArray[i] = booleanArray[i] ? 1 : 0;

		return integerArray;
	}

	boolean[] truncate()
	{
		int truncable = booleanArray.length;
		while (truncable > 1 && !booleanArray[truncable - 1])
			truncable--;

		return Arrays.copyOf(booleanArray, truncable);
	}


	boolean[] expand(int newLength)
	{
		return Arrays.copyOf(booleanArray, newLength);
	}


	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder(booleanArray.length);
		sb.append(booleanArray[0] ? "+" : "-");

		for (int i = booleanArray.length - 1; i >= 1; i--)                // reversely printed
			sb.append(booleanArray[i] ? "1" : "0");

		return sb.toString();
	}


	public int toInteger()
	{
		int dualPotency = booleanArray[0] ? 1 : -1;
		int result = 0;

		for (int p = 1; p < booleanArray.length; p++)
		{
			if (booleanArray[p])
				result += dualPotency;

			dualPotency *= 2;
		}

		return result;
	}
}
