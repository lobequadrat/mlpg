package com.mlpg.examples.playground;

import com.google.gson.Gson;

import java.io.Serial;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Staff
{

	private String name;
	private int age;
	private String[] position;              // array
	private List<String> skills;            // list
	private Map<String, BigDecimal> salary; // map

	public static Staff createStaffObject()
	{

		Staff staff = new Staff();

		staff.name = "mkyong";
		staff.age = 35;
		staff.position = new String[]{"Founder", "CTO", "Writer"};

		Map<String, BigDecimal> salary = new HashMap<>()
		{
			@Serial
			private static final long serialVersionUID = 1L;

			{
				put("2010", new BigDecimal(10000));
				put("2012", new BigDecimal(12000));
				put("2018", new BigDecimal(14000));
			}
		};
		staff.salary = salary;
		staff.skills = Arrays.asList("java", "python", "node", "kotlin");

		return staff;

	}


	public static void main(String[] args)
	{
		Gson gson = new Gson();

		Staff staff = createStaffObject();

		//InputPerzeptron test = new InputPerzeptron("blabla", 0.23);

		// Java objects to String
		// String json = gson.toJson(staff);

		// Java objects to File
//      try (FileWriter writer = new FileWriter("staff.json"))
//      {
//          gson.toJson(test, writer);
//      } catch (IOException e)
//      {
//          e.printStackTrace();
//      }

	}

}
