package com.mlpg.examples.playground;

import com.google.gson.Gson;
import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;

public final class TestSerialize
{
	private TestSerialize()
	{
	}

	public static void main(String[] args)
	{
		NeuralNetworkHillClimbing nn = new NeuralNetworkHillClimbing();
		InputNeuron n1 = new InputNeuron(nn, "n1", 0);

		//Neuron n2 = new HiddenPerzeptron("N2", 0.3);
		// n2.connect(n1, 0.3);

		Gson gson = new Gson();
		try (FileWriter writer = new FileWriter("nn.json"))
		{
			gson.toJson(nn, writer);
			//gson.toJson(n2, writer);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}


		try (Reader reader = new FileReader("nn.json"))
		{

			NeuralNetworkHillClimbing nnout = gson.fromJson(reader, NeuralNetworkHillClimbing.class);
			//HiddenPerzeptron hidden = gson.fromJson(reader, HiddenPerzeptron.class);
			for (Neuron i : nnout.getInputNeurons())
				i.setNetwork(nnout);

			System.out.println(nnout);
			// System.out.println(hidden);
			// String json = "{'name' : 'mkyong'}";
			// Object object = gson.fromJson(json, Staff.class);


		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
