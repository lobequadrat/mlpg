package com.mlpg.examples.playground;

//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Random;

public class NeuralNetwork1BackpropagationTest
{
	/*
	private static final int TRAINING_CYCLES = 100;

	private static final int INITIALINPUTVALUE = 0;

	private static final double W1 = 0.15;
	private static final double W2 = 0.2;
	private static final double W3 = 0.25;
	private static final double W4 = 0.3;

	private static final double W5 = 0.4;
	private static final double W6 = 0.45;
	private static final double W7 = 0.5;
	private static final double W8 = 0.55;

	private static final double BIAS_1 = 0.35;
	private static final double BIAS_2 = 0.6;

	private static final Double TARGET_VALUE_O1 = 0.01;
	private static final Double TARGET_VALUE_O2 = 0.99;

	private static ArrayList<Neuron> allNeurons = new ArrayList<Neuron>();
	private static ArrayList<HiddenOutputNeuron> allNeuronsWithInputs = new ArrayList<HiddenOutputNeuron>();

	private static InputNeuron i1 = new InputNeuron(INITIALINPUTVALUE, "I1", allNeurons);
	private static InputNeuron i2 = new InputNeuron(INITIALINPUTVALUE, "I2", allNeurons);

	private static HiddenOutputPerzeptron h1 = new HiddenOutputPerzeptron(BIAS_1, "H1", allNeurons, allNeuronsWithInputs);
	private static HiddenOutputPerzeptron h2 = new HiddenOutputPerzeptron(BIAS_1, "H1", allNeurons, allNeuronsWithInputs);

	private static OutputPerzeptron o1 = new OutputPerzeptron(BIAS_2, "O1", allNeurons, allNeuronsWithInputs);
	private static OutputPerzeptron o2 = new OutputPerzeptron(BIAS_2, "O2", allNeurons, allNeuronsWithInputs);



	public static void main (String [] args)
	{
		h1.connectInput(i1, W1);
		h1.connectInput(i2, W2);
		h2.connectInput(i1, W3);
		h2.connectInput(i2, W4);

		o1.connectInput(h1, W5);
		o1.connectInput(h2, W6);
		o2.connectInput(h1, W7);
		o2.connectInput(h2, W8);

		allNeuronsWithInputs.add(h1);
		allNeuronsWithInputs.add(h2);
		allNeuronsWithInputs.add(o1);
		allNeuronsWithInputs.add(o1);

		for (int i = 0; i < TRAINING_CYCLES; i++)
		{
			System.out.println("Net error = " + tryToClimbHill());
		}

		simulateNetwork();
	}


	private static double tryToClimbHill()
	{
		double oldErrorSum = TARGET_VALUE_O1 - o1.getOutput() + TARGET_VALUE_O2 - o2.getOutput();
		HashMap<HiddenOutputNeuron, HashMap<Neuron, Double>> inputsNeurons2Inputs2Weights = new HashMap<HiddenOutputNeuron, HashMap<Neuron, Double>>();

		// change position (in the mist on the hill)
		for (HiddenOutputNeuron n : allNeuronsWithInputs)
			inputsNeurons2Inputs2Weights.put(n, n.getWeightsAndGuessNewWeights());

		simulateNetwork();
		double newErrorSum = TARGET_VALUE_O1 - o1.getOutput() + TARGET_VALUE_O2 - o2.getOutput();

		// we are heading down hill ... go back!
		if (newErrorSum > oldErrorSum)
		{
			for (HiddenOutputNeuron n : inputsNeurons2Inputs2Weights.keySet())
			{
				for (Neuron i : inputsNeurons2Inputs2Weights.get(n).keySet())
					n.changeWeight(i, inputsNeurons2Inputs2Weights.get(n).get(i));
			}
		}

		return newErrorSum;
	}

	private static void simulateNetwork()
	{
		o1.simulate();
		o2.simulate();
	}
	*/
}
