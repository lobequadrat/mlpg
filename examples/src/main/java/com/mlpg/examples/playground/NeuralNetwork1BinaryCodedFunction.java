package com.mlpg.examples.playground;

// import javax.swing.JFrame;

// import org.math.plot.Plot2DPanel;

//import de.lingen.modsim.nn.core.core.InputNeuron;
//import de.lingen.modsim.nn.perzeptron.OutputPerzeptron;

public final class NeuralNetwork1BinaryCodedFunction
{
	static final int TRAINING_CYCLES = 100;
	static final int MINX = -100;
	static final int MAXX = 100;
	static final double THRESHOLD = 0.5;
	static final double WEIGHT = 0.7;
	static final double LEARNINGRATE = 0.001;

	// static Plot2DPanel plot = new Plot2DPanel();

	private NeuralNetwork1BinaryCodedFunction()
	{
	}

	public static void main(String[] args)
	{
		/*
		create input neurons
		DualNumber dMINX = new DualNumber(MINX);
		DualNumber dMAXX = new DualNumber(MAXX);
		int dXSize = Math.max(dMINX.number.length, dMAXX.number.length);
		InputNeuron[] inputNeurons = new InputNeuron[ dXSize ];
		for (int x = 0; x < inputNeurons.length; x++)
			inputNeurons[x] = new InputNeuron(0, "IN" + x);

		int minY = Integer.MAX_VALUE;
		int maxY = Integer.MIN_VALUE;
		int y = 0;
		for (int x = MINX; x <= MAXX; x++)
		{
			y = function(x);
			minY = Math.min(minY, y);
			maxY = Math.max(maxY, y);
		}

		// create output neurons
		DualNumber dMINY = new DualNumber(minY);
		DualNumber dMAXY = new DualNumber(maxY);
		int dYSize = Math.max(dMINY.number.length, dMAXY.number.length);
		OutputPerzeptron[] outputNeurons = new OutputPerzeptron[ dYSize ];
		for (y = 0; y < outputNeurons.length; y++)
		{
			OutputPerzeptron n = new OutputPerzeptron(THRESHOLD, "N" + y, LEARNINGRATE);

			for (int x = 0; x < inputNeurons.length; x++)
				n.connectInput(inputNeurons[x], WEIGHT);

			outputNeurons[y] = n;
		}

		// training
		for (int t = 0; t < TRAINING_CYCLES; t++)
		{
			double netError = 0;

			for (int x = MINX; x <= MAXX; x++)
			{
				boolean[] dX = new DualNumber(x).expand(dXSize);
				for (int i = 0; i < dX.length; i++)
					inputNeurons[i].changeInput(dX[i] ? 1 : 0);


				boolean[] dY = new DualNumber(function(x)).expand(dYSize);
				for (int i = 0; i < outputNeurons.length; i++)
				{
					outputNeurons[i].simulate();
					double error = (dY[i] ? 1 : 0) - outputNeurons[i].getOutput();
					outputNeurons[i].setError(error);
					outputNeurons[i].train();

					netError += error * error;
				}
			}

			System.out.println("Net error = " + netError);
		}

		System.out.println("Net size : " + (dXSize + dYSize));

		// plotting values
		double[] xValues = new double[MAXX - MINX + 1];
		double[] yValues = new double[maxY - minY + 1];
		double[] yValuesLearned = new double[maxY - minY + 1];

		int i = 0;
		for (int x = MINX; x <= MAXX; x++)
		{
			xValues[i] = x;
			yValues[i] = function(x);

			boolean[] dX = new DualNumber(x).expand(dXSize);
			for (int i2 = 0; i2 < dX.length; i2++)
				inputNeurons[i2].changeInput(dX[i2] ? 1 : 0);

			DualNumber dY = new DualNumber(0);
			dY.number = dY.expand(dYSize);
			for (int i2 = 0; i2 < outputNeurons.length; i2++)
			{
				outputNeurons[i2].simulate();
				dY.number[i2] = outputNeurons[i2].getFired();
			}

			yValuesLearned[i] = dY.toInteger();
			i++;
		}

		// plotting
		plot.addLegend("SOUTH");

		plot.addLinePlot("Original", xValues, yValues);
		plot.addLinePlot("Learned", xValues, yValuesLearned);

		JFrame frame = new JFrame("The plot panel");
		frame.setSize(600, 600);
		frame.setContentPane(plot);
		frame.setVisible(true);
		*/
	}
}
