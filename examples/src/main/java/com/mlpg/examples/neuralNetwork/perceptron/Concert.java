package com.mlpg.examples.neuralNetwork.perceptron;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.Parameters;
import com.mlpg.methods.neuralNetwork.hillClimbing.HiddenNeuronHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.OutputNeuronHillClimbing;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.OutputCheck;
import com.mlpg.methods.util.xml.XMLWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;

/* Testcase w1: i1 --> o1 = 0.5
 			w2: i2 --> o1 = 0.5
 			b1: o1 = -0.51
*/

public final class Concert
{
	private static Double error;

	private Concert()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		// TODO:
		// bias can fire perzeptron (constantly) without input?
		// randomize weights for testing

		NeuralNetworkHillClimbing nn = new NeuralNetworkHillClimbing();
		Neuron n1 = new InputNeuron(nn, "N1", INPUT_TRUE);
		Neuron n2 = new InputNeuron(nn, "N2", INPUT_TRUE);
		Neuron n3 = new InputNeuron(nn, "N3", INPUT_TRUE);

		Neuron n4 = new HiddenNeuronHillClimbing(nn, "N4").bias(0.2);
		Neuron n5 = new HiddenNeuronHillClimbing(nn, "N5").bias(0.2);

		OutputNeuronHillClimbing n6 = new OutputNeuronHillClimbing(nn, "N6").bias();

		n4.connectInput(n1, 0.3);
		n4.connectInput(n2, 0.4);
		n4.connectInput(n3, -0.7);

		n5.connectInput(n1, -0.8);
		n5.connectInput(n2, -0.5);
		n5.connectInput(n3, 0.7);

		n6.connectInput(n4, 0.4);
		n6.connectInput(n5, 0.6);

		nn.computeLayers();

		for (int i = 1; i < Parameters.TRAININGCYCLES & !printResultAndDetermineIfDone(nn, n6, i); i++)
			nn.train(error);

		try
		{
			System.out.println("Working Directory = " + System.getProperty("user.dir"));
			XMLWriter.saveNetwork(nn, "test.xml");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		ManageNN.tearDown();
	}


	private static boolean printResultAndDetermineIfDone(NeuralNetworkHillClimbing nn, Neuron n6, int iteration)
	{
		StringBuilder fineLog = new StringBuilder();

		OutputCheck<Boolean> outputCheck;
		outputCheck = new OutputCheck<>();

		error = nn.setInputs(INPUT_FALSE, INPUT_FALSE, INPUT_FALSE)
				.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(n6.getFired(), false);
		error += nn.setInputs(INPUT_FALSE, INPUT_FALSE, INPUT_TRUE)
				.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(n6.getFired(), false);
		error += nn.setInputs(INPUT_FALSE, INPUT_TRUE, INPUT_FALSE)
				.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(n6.getFired(), false);
		error += nn.setInputs(INPUT_TRUE, INPUT_FALSE, INPUT_FALSE)
				.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(n6.getFired(), false);
		error += nn.setInputs(INPUT_TRUE, INPUT_FALSE, INPUT_TRUE)
				.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(n6.getFired(), false);
		error += nn.setInputs(INPUT_TRUE, INPUT_TRUE, INPUT_FALSE)
				.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(n6.getFired(), false);
		error += nn.setInputs(INPUT_FALSE, INPUT_TRUE, INPUT_TRUE)
				.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(n6.getFired(), false);
		error += nn.setInputs(INPUT_TRUE, INPUT_TRUE, INPUT_TRUE)
				.forwardPropagation().setTargetOutputs(OUTPUT_TRUE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(n6.getFired(), true);

		Parameters.LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ")
				.concat(Parameters.getDfl().format(error)) + "\n" + fineLog.toString());

		return outputCheck.allNonEqualValues() <= 0;
	}
}
