package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import com.mlpg.methods.neuralNetwork.deeplearning.HiddenNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.csv.CSVWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class XOR
{
	private XOR()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid());


		// Testcase
		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		/*
		// bias and weights need to be high because of activation function
		Neuron h1 = new HiddenNeuronDeepLearning(nn, "H1").bias(-10);
		Neuron h2 = new HiddenNeuronDeepLearning(nn, "H2").bias(-10);
		h1.connectInput(i1, -90);
		h1.connectInput(i2, 100);
		h2.connectInput(i1, 100);
		h2.connectInput(i2, -90);

		OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1").bias(-10);
		o1.connectInput(h1, 100);
		o1.connectInput(h2, 100);
		*/

		Neuron h1 = new HiddenNeuronDeepLearning(nn, "H1").bias();
		Neuron h2 = new HiddenNeuronDeepLearning(nn, "H2").bias();
		h1.connectInput(i1);
		h1.connectInput(i2);
		h2.connectInput(i1);
		h2.connectInput(i2);

		OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1").bias();
		o1.connectInput(h1);
		o1.connectInput(h2);

		nn.computeLayers();
		nn.setSizeInputDataSet(4);

		int iteration = 0;
		double sumErrors;
		do
		{
			StringBuilder fineLog = null; // new StringBuilder();
			iteration++;

			sumErrors = nn.train(nn.setInputs(INPUT_FALSE, INPUT_FALSE)
					.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			sumErrors += nn.train(nn.setInputs(INPUT_TRUE, INPUT_FALSE)
					.forwardPropagation().setTargetOutputs(OUTPUT_TRUE).getNetError(fineLog));
			sumErrors += nn.train(nn.setInputs(INPUT_FALSE, INPUT_TRUE)
					.forwardPropagation().setTargetOutputs(OUTPUT_TRUE).getNetError(fineLog));
			sumErrors += nn.train(nn.setInputs(INPUT_TRUE, INPUT_TRUE)
					.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));

			LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(sumErrors))); // + "\n" + fineLog.toString());
		} while (iteration < TRAININGCYCLES && Math.abs(sumErrors) >= ERROR_MARGIN_NET);

		writeResultsToFile(nn, o1, "output_XOR.csv");
		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}

	private static void writeResultsToFile(NeuralNetwork nn, Neuron o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:False values written: " + w.writeData(nn, o1, "NNFalse", false));
		LOGGER.info("NN:True values written: " + w.writeData(nn, o1, "NNTrue", true));

		// sample training
		w.writeHeader("XORFalse", "input1", "input2", "output");
		w.writeLine(INPUT_FALSE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_TRUE, INPUT_TRUE, OUTPUT_FALSE);
		w.writeHeader("XORTrue", "input1", "input2", "output");
		w.writeLine(INPUT_FALSE, INPUT_TRUE, OUTPUT_TRUE);
		w.writeLine(INPUT_TRUE, INPUT_TRUE, OUTPUT_TRUE);
		w.close();
	}
}
