package com.mlpg.examples.neuralNetwork.perceptronContinuous;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.hillClimbing.HiddenNeuronHillClimbingContinuous;
import com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.OutputNeuronHillClimbingContinuous;
import com.mlpg.methods.util.Box;
import com.mlpg.methods.util.LabelledPoint;
import com.mlpg.methods.util.LabelledPointsAndBoxes;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.csv.CSVWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class XYBox7Neurons
{
	private static Double error;

	private XYBox7Neurons()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkHillClimbing nn = new NeuralNetworkHillClimbing();
		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		/*
		// Testcase
		PerzeptronHidden h1 = new PerzeptronHidden(nn, "H1").bias(-0.249);
		h1.connectInput(i1, 1);
		PerzeptronHidden h2 = new PerzeptronHidden(nn, "H2").bias(0.75);
		h2.connectInput(i1, -1);
		PerzeptronHidden h3 = new PerzeptronHidden(nn, "H3").bias(-0.249);
		h3.connectInput(i2, 1);
		PerzeptronHidden h4 = new PerzeptronHidden(nn, "H4").bias(0.75);
		h4.connectInput(i2, -1);

		PerzeptronHidden h5 = new PerzeptronHidden(nn, "H5").bias(-0.75);
		h5.connectInput(h1, 0.5);
		h5.connectInput(h2, 0.5);
		PerzeptronHidden h6 = new PerzeptronHidden(nn, "H6").bias(-0.75);
		h6.connectInput(h3, 0.5);
		h6.connectInput(h4, 0.5);

		PerzeptronOutput o1 = new PerzeptronOutput(nn, "O1").bias(-0.75);
		o1.connectInput(h5, 0.5);
		o1.connectInput(h6, 0.5);
		 */

		HiddenNeuronHillClimbingContinuous h1 = new HiddenNeuronHillClimbingContinuous(nn, "H1").bias();
		h1.connectInput(i1);
		HiddenNeuronHillClimbingContinuous h2 = new HiddenNeuronHillClimbingContinuous(nn, "H2").bias();
		h2.connectInput(i1);
		HiddenNeuronHillClimbingContinuous h3 = new HiddenNeuronHillClimbingContinuous(nn, "H3").bias();
		h3.connectInput(i2);
		HiddenNeuronHillClimbingContinuous h4 = new HiddenNeuronHillClimbingContinuous(nn, "H4").bias();
		h4.connectInput(i2);

		HiddenNeuronHillClimbingContinuous h5 = new HiddenNeuronHillClimbingContinuous(nn, "H5").bias();
		h5.connectInput(h1);
		h5.connectInput(h2);
		HiddenNeuronHillClimbingContinuous h6 = new HiddenNeuronHillClimbingContinuous(nn, "H6").bias();
		h6.connectInput(h3);
		h6.connectInput(h4);

		OutputNeuronHillClimbingContinuous o1 = new OutputNeuronHillClimbingContinuous(nn, "O1").bias();
		o1.connectInput(h5);
		o1.connectInput(h6);

		nn.computeLayers();

		LabelledPointsAndBoxes.getBoxes().add(new Box(0.25, 0.25, 0.75, 0.75));
		LabelledPointsAndBoxes trainingPoints = new LabelledPointsAndBoxes(400, o1, true);
		// LabelledPointsAndBoxes evaluationPoints = new LabelledPointsAndBoxes(300, true);

		for (int i = 1; i < TRAININGCYCLES & !printResultAndDetermineIfDone(nn, trainingPoints, o1, i); i++)
			nn.train(error);

		writeResultsToFile(nn, trainingPoints, o1, "output_BOX.csv");
		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}


	private static void writeResultsToFile(NeuralNetworkHillClimbing nn, LabelledPointsAndBoxes trainingPoints, Neuron o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:False values written: " + w.writeData(nn, o1, "NNFalse", false));
		LOGGER.info("NN:TRUE values written: " + w.writeData(nn, o1, "NNTrue", true));

		// sample training
		w.writeHeader("BOXFalse", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
			if (lp.getLabel() == OUTPUT_FALSE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_FALSE);

		// sample training
		w.writeHeader("BOXTrue", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
			if (lp.getLabel() == OUTPUT_TRUE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_TRUE);

		w.close();
	}

	private static boolean printResultAndDetermineIfDone(NeuralNetworkHillClimbing nn, LabelledPointsAndBoxes trainingPoints, Neuron o1, int iteration)
	{
		StringBuilder fineLog = new StringBuilder();

		error = 0.0;
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
			error += getError(nn, lp.getX(), lp.getY(), lp.getLabel(), null);

		LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(error)) + "\n" + fineLog.toString());

		return error <= ERROR_MARGIN_NET;
	}

	private static double getError(NeuralNetworkHillClimbing nn, double input1, double input2, double output, StringBuilder fineLog)
	{
		nn.setInputs(input1, input2);
		nn.forwardPropagation();
		double errorTmp = nn.getNetError(fineLog);

		if (fineLog != null)
			fineLog.append("\t" + nn.toString(true).concat(", Error: ").concat(getDfl().format(error)) + "\n");

		return errorTmp;
	}
}
