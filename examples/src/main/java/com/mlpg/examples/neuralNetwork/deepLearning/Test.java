package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.deeplearning.HiddenNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.ManageNN;

import static com.mlpg.methods.neuralNetwork.Parameters.LOGGER;


public final class Test
{
	private Test()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid(), 0.5);

		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		Neuron h1 = new HiddenNeuronDeepLearning(nn, "H1").bias(0.35);
		h1.connectInput(i1, 0.15);
		h1.connectInput(i2, 0.2);

		Neuron h2 = new HiddenNeuronDeepLearning(nn, "H2").bias(0.35);
		h2.connectInput(i1, 0.25);
		h2.connectInput(i2, 0.3);

		OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1").bias(0.6);
		o1.connectInput(h1, 0.4);
		o1.connectInput(h2, 0.45);

		OutputNeuronDeepLearning o2 = new OutputNeuronDeepLearning(nn, "O2").bias(0.6);
		o2.connectInput(h1, 0.5);
		o2.connectInput(h2, 0.55);

		nn.computeLayers();
		nn.setSizeInputDataSet(1);

		double error = nn.setInputs(0.05, 0.1).forwardPropagation().setTargetOutputs(0.01, 0.99).getNetError(null);

		LOGGER.info("error : " + error);
		nn.train(error);

		LOGGER.info("w5: " + nn.getInputSynapses().get(o1).get(h1));
		LOGGER.info("w6: " + nn.getInputSynapses().get(o1).get(h2));
		LOGGER.info("w7: " + nn.getInputSynapses().get(o2).get(h1));
		LOGGER.info("w8: " + nn.getInputSynapses().get(o2).get(h2));

		LOGGER.info("w1: " + nn.getInputSynapses().get(h1).get(i1));
		LOGGER.info("w2: " + nn.getInputSynapses().get(h1).get(i2));
		LOGGER.info("w3: " + nn.getInputSynapses().get(h2).get(i1));
		LOGGER.info("w4: " + nn.getInputSynapses().get(h2).get(i2));

		error = nn.forwardPropagation().getNetError(null);
		LOGGER.info("error : " + error);

		for (int i = 0; i < 999; i++)
			error = nn.train(nn.forwardPropagation().getNetError(null));

		LOGGER.info("error : " + error);

//		 LOGGER.info( nn.toString().concat(", Error: ").concat(getDfl().format(error)) + "\n" + fineLog.toString());

		ManageNN.tearDown();
	}
}
