package com.mlpg.examples.neuralNetwork.perceptron;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.hillClimbing.HiddenNeuronHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.OutputNeuronHillClimbing;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.OutputCheck;
import com.mlpg.methods.util.csv.CSVWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class XOR
{
	private static Double error;

	private XOR()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkHillClimbing nn = new NeuralNetworkHillClimbing();
		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		/*
		// Testcase
		Neuron h1 = new PerzeptronHidden(nn, "H1").bias(-0.1);
		h1.connectInput(i1, 0.5);
		h1.connectInput(i2, -0.5);

		Neuron h2 = new PerzeptronHidden(nn, "H2").bias(-0.1);
		h2.connectInput(i1, -0.5);
		h2.connectInput(i2, 0.5);


		PerzeptronOutput o1 = new PerzeptronOutput(nn, "O1").bias(-0.1);
		o1.connectInput(h1, 0.5);
		o1.connectInput(h2, 0.5);
		*/

		Neuron h1 = new HiddenNeuronHillClimbing(nn, "H1").bias();
		h1.connectInput(i1);
		h1.connectInput(i2);

		Neuron h2 = new HiddenNeuronHillClimbing(nn, "H2").bias();
		h2.connectInput(i1);
		h2.connectInput(i2);

		OutputNeuronHillClimbing o1 = new OutputNeuronHillClimbing(nn, "O1").bias();
		o1.connectInput(h1);
		o1.connectInput(h2);

		nn.computeLayers();

		for (int i = 1; i < TRAININGCYCLES & !printResultAndDetermineIfDone(nn, o1, i); i++)
			nn.train(error);

		writeResultsToFile(nn, o1, "output_XOR.csv");
		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}

	private static void writeResultsToFile(NeuralNetworkHillClimbing nn, OutputNeuronHillClimbing o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:False values written: " + w.writeData(nn, o1, "NNFalse", false));
		LOGGER.info("NN:False values written: " + w.writeData(nn, o1, "NNTrue", true));

		// sample training
		w.writeHeader("XORFalse", "input1", "input2", "output");
		w.writeLine(INPUT_FALSE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_TRUE, INPUT_TRUE, OUTPUT_FALSE);

		w.writeHeader("XORTrue", "input1", "input2", "output");
		w.writeLine(INPUT_TRUE, INPUT_FALSE, OUTPUT_TRUE);
		w.writeLine(INPUT_FALSE, INPUT_TRUE, OUTPUT_TRUE);

		w.close();
	}

	private static boolean printResultAndDetermineIfDone(NeuralNetworkHillClimbing nn, Neuron o1, int iteration)
	{
		OutputCheck<Boolean> outputCheck;
		outputCheck = new OutputCheck<>();
		StringBuilder fineLog = new StringBuilder();

		error = nn.setInputs(INPUT_FALSE, INPUT_FALSE).forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), false);
		error += nn.setInputs(INPUT_TRUE, INPUT_FALSE).forwardPropagation().setTargetOutputs(OUTPUT_TRUE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), true);
		error += nn.setInputs(INPUT_FALSE, INPUT_TRUE).forwardPropagation().setTargetOutputs(OUTPUT_TRUE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), true);
		error += nn.setInputs(INPUT_TRUE, INPUT_TRUE).forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), false);

		LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(error)) + "\n" + fineLog.toString());
		return outputCheck.allNonEqualValues() <= 0;
	}
}
