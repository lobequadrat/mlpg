package com.mlpg.examples.neuralNetwork.perceptronContinuous;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.hillClimbing.HiddenNeuronHillClimbingContinuous;
import com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.OutputNeuronHillClimbingContinuous;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.OutputCheck;
import com.mlpg.methods.util.csv.CSVWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class XOR
{
	private static Double error;

	private XOR()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkHillClimbing nn = new NeuralNetworkHillClimbing();
		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		/*
		// Testcase

		Neuron h1 = new PerzeptronHidden(nn, "H1", -0.1);
		Neuron h2 = new PerzeptronHidden(nn, "H2", -0.1);

		PerzeptronOutput o1 = new PerzeptronOutput(nn, "O1", -0.1);

		h1.connectInput(i1, 0.5);
		h1.connectInput(i2, -0.5);

		h2.connectInput(i1, -0.5);
		h2.connectInput(i2, 0.5);

		o1.connectInput(h1, 0.5);
		o1.connectInput(h2, 0.5);
		*/

		Neuron h1 = new HiddenNeuronHillClimbingContinuous(nn, "H1").bias();
		Neuron h2 = new HiddenNeuronHillClimbingContinuous(nn, "H2").bias();

		OutputNeuronHillClimbingContinuous o1 = new OutputNeuronHillClimbingContinuous(nn, "O1").bias();

		h1.connectInput(i1);
		h1.connectInput(i2);

		h2.connectInput(i1);
		h2.connectInput(i2);

		o1.connectInput(h1);
		o1.connectInput(h2);

		nn.computeLayers();

		for (int i = 1; i < TRAININGCYCLES & !printResultAndDetermineIfDone(nn, o1, i); i++)
			nn.train(error);

		writeResultsToFile(nn, o1, "output_XORContinuous.csv");
		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}

	private static void writeResultsToFile(NeuralNetworkHillClimbing nn, Neuron o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:Continuous values written: " + w.writeData(nn, o1, "NNContinuous", null));

		// sample training
		/* w.writeHeader("XORFalse", "input1", "input2", "output");
		w.writeLine(INPUT_FALSE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_TRUE, INPUT_TRUE, OUTPUT_FALSE);

		w.writeHeader("XORTrue", "input1", "input2", "output");
		w.writeLine(INPUT_TRUE, INPUT_FALSE, OUTPUT_TRUE);
		w.writeLine(INPUT_FALSE, INPUT_TRUE, OUTPUT_TRUE);
		*/

		w.close();
	}

	private static boolean printResultAndDetermineIfDone(NeuralNetworkHillClimbing nn, Neuron o1, int iteration)
	{
		OutputCheck<Boolean> outputCheck;
		outputCheck = new OutputCheck<>();
		StringBuilder fineLog = new StringBuilder();

		error = getError(nn, INPUT_FALSE, INPUT_FALSE, OUTPUT_FALSE, fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), false);
		error += getError(nn, INPUT_TRUE, INPUT_FALSE, OUTPUT_TRUE, fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), true);
		error += getError(nn, INPUT_FALSE, INPUT_TRUE, OUTPUT_TRUE, fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), true);
		error += getError(nn, INPUT_TRUE, INPUT_TRUE, OUTPUT_FALSE, fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), false);

		LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(error)) + "\n" + fineLog.toString());
		return outputCheck.allNonEqualValues() <= 0;
	}

	private static double getError(NeuralNetworkHillClimbing nn, double input1, double input2, double output, StringBuilder fineLog)
	{
		nn.setInputs(input1, input2);
		nn.forwardPropagation();
		double errorTmp = nn.getNetError(fineLog);

		if (fineLog != null)
			fineLog.append("\t" + nn.toString(true).concat(", Error: ").concat(getDfl().format(error)) + "\n");

		return errorTmp;
	}
}
