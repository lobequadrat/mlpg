package com.mlpg.examples.neuralNetwork.perceptron;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.Parameters;
import com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.OutputNeuronHillClimbing;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.OutputCheck;

/* Testcase w1: i1 --> o1 = 0.5
 			w2: i2 --> o1 = 0.5
 			b1: o1 = -0.51
*/
public final class NOT
{
	private static Double error;

	private NOT()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkHillClimbing nn = new NeuralNetworkHillClimbing();
		Neuron i1 = new InputNeuron(nn, "I1");

		/*  Testcase
		PerzeptronOutput o1 = new PerzeptronOutput(nn, "O1").bias(0.5);
		o1.connectInput(i1, -0.51);
		 */

		OutputNeuronHillClimbing o1 = new OutputNeuronHillClimbing(nn, "O1").bias();
		o1.connectInput(i1);
		nn.computeLayers();

		for (int i = 1; i < Parameters.TRAININGCYCLES & !printResultAndDetermineIfDone(nn, o1, i); i++)
			nn.train(error);

		Parameters.LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}

	private static boolean printResultAndDetermineIfDone(NeuralNetworkHillClimbing nn, Neuron o1, int iteration)
	{
		OutputCheck<Boolean> outputCheck;
		outputCheck = new OutputCheck<>();

		StringBuilder fineLog = new StringBuilder();

		error = nn.setInputs(Parameters.INPUT_FALSE).forwardPropagation().setTargetOutputs(Parameters.OUTPUT_TRUE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), false);
		error += nn.setInputs(Parameters.INPUT_TRUE).forwardPropagation().setTargetOutputs(Parameters.OUTPUT_FALSE).getNetError(fineLog);
		outputCheck.addComputedAndToTrain(o1.getFired(), true);

		Parameters.LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ")
				.concat(Parameters.getDfl().format(error)) + "\n" + fineLog.toString());
		return outputCheck.allNonEqualValues() <= 0;
	}

	private static double getError(NeuralNetworkHillClimbing nn, double input1, double output, StringBuilder fineLog)
	{
		double errorTmp;
		nn.setInputs(input1);
		nn.forwardPropagation();
		errorTmp = nn.getNetError(fineLog);

		if (fineLog != null)
			fineLog.append("\t" + nn.toString(true).concat(", Error: ").concat(Parameters.getDfl().format(errorTmp)) + "\n");

		return errorTmp;
	}
}
