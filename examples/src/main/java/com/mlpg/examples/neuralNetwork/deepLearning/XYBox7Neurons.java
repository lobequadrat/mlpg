package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.deeplearning.HiddenNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.Box;
import com.mlpg.methods.util.LabelledPoint;
import com.mlpg.methods.util.LabelledPointsAndBoxes;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.csv.CSVWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class XYBox7Neurons
{
	private XYBox7Neurons()
	{
	}

	public static void main(String[] args) throws Exception
	{
		ManageNN.setup();

		NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid());
		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		HiddenNeuronDeepLearning h1 = new HiddenNeuronDeepLearning(nn, "H1").bias();
		h1.connectInput(i1);
		HiddenNeuronDeepLearning h2 = new HiddenNeuronDeepLearning(nn, "H2").bias();
		h2.connectInput(i1);
		HiddenNeuronDeepLearning h3 = new HiddenNeuronDeepLearning(nn, "H3").bias();
		h3.connectInput(i2);
		HiddenNeuronDeepLearning h4 = new HiddenNeuronDeepLearning(nn, "H4").bias();
		h4.connectInput(i2);

		HiddenNeuronDeepLearning h5 = new HiddenNeuronDeepLearning(nn, "H5").bias();
		h5.connectInput(h1);
		h5.connectInput(h2);
		HiddenNeuronDeepLearning h6 = new HiddenNeuronDeepLearning(nn, "H6").bias();
		h6.connectInput(h3);
		h6.connectInput(h4);

		OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1").bias();
		o1.connectInput(h5);
		o1.connectInput(h6);

		nn.computeLayers();
		nn.setSizeInputDataSet(400);

		LabelledPointsAndBoxes.getBoxes().add(new Box(0.25, 0.25, 0.75, 0.75));
		LabelledPointsAndBoxes trainingPoints = new LabelledPointsAndBoxes(400, o1, true);
		// LabelledPointsAndBoxes evaluationPoints = new LabelledPointsAndBoxes(300, true);

		String filePrefix = "XYBox7Neurons";
		Integer fileSuffix = 0;
		int snapshoteveryXTrainings = 10000;
		//XMLWriter.saveNetwork(nn, filePrefix + fileSuffix++);
		int iteration = 0;
		double netError;
		do
		{
			StringBuilder fineLog = null; // new StringBuilder();

			netError = 0.0;
			for (LabelledPoint lp : trainingPoints.getLabelledPoints())
				netError += nn.setInputs(lp.getX(), lp.getY()).forwardPropagation().setTargetOutputs(lp.getLabel()).getNetError(null);

			LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(netError))); // + "\n" + fineLog.toString());
			nn.train(netError);

			// if (snapshoteveryXTrainings % snapshoteveryXTrainings == 0)
			//	XMLWriter.saveNetwork(nn, filePrefix + fileSuffix++);

			iteration++;
		} while (iteration < TRAININGCYCLES && Math.abs(netError) >= ERROR_MARGIN_NET);

		writeResultsToFile(nn, trainingPoints, o1, "output_BOX.csv");
		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}

	private static void writeResultsToFile(
			NeuralNetworkDeepLearning nn, LabelledPointsAndBoxes trainingPoints, OutputNeuronDeepLearning o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:False values written: " + w.writeData(nn, o1, "NNFalse", false));
		LOGGER.info("NN:TRUE values written: " + w.writeData(nn, o1, "NNTrue", true));

		// sample training
		w.writeHeader("BOXFalse", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
			if (lp.getLabel() == OUTPUT_FALSE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_FALSE);

		// sample training
		w.writeHeader("BOXTrue", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
			if (lp.getLabel() == OUTPUT_TRUE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_TRUE);

		w.close();
	}
}
