package com.mlpg.examples.neuralNetwork.perceptron;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.hillClimbing.HiddenNeuronHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.OutputNeuronHillClimbing;
import com.mlpg.methods.util.Box;
import com.mlpg.methods.util.LabelledPoint;
import com.mlpg.methods.util.LabelledPointsAndBoxes;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.csv.CSVWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class XYBox3Neurons
{
	private static Double error;

	private XYBox3Neurons()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkHillClimbing nn = new NeuralNetworkHillClimbing();
		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		HiddenNeuronHillClimbing h1 = new HiddenNeuronHillClimbing(nn, "H1").bias();
		h1.connectInput(i1);
		h1.connectInput(i2);

		HiddenNeuronHillClimbing h2 = new HiddenNeuronHillClimbing(nn, "H2").bias();
		h2.connectInput(i1);
		h2.connectInput(i2);

		OutputNeuronHillClimbing o1 = new OutputNeuronHillClimbing(nn, "O1").bias();
		o1.connectInput(h1);
		o1.connectInput(h2);

		nn.computeLayers();

		LabelledPointsAndBoxes.getBoxes().add(new Box(0.25, 0.25, 0.75, 0.75));
		LabelledPointsAndBoxes trainingPoints = new LabelledPointsAndBoxes(500, o1, true);
		// LabelledPointsAndBoxes evaluationPoints = new LabelledPointsAndBoxes(300, true);

		for (int i = 1; i < TRAININGCYCLES & !printResultAndDetermineIfDone(nn, trainingPoints, o1, i); i++)
			nn.train(error);

		writeResultsToFile(nn, trainingPoints, o1, "output_BOX.csv");
		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
	}


	private static void writeResultsToFile(
			NeuralNetworkHillClimbing nn, LabelledPointsAndBoxes trainingPoints, OutputNeuronHillClimbing o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:False values written: " + w.writeData(nn, o1, "NNFalse", false));
		LOGGER.info("NN:TRUE values written: " + w.writeData(nn, o1, "NNTrue", true));

		// sample training
		w.writeHeader("BOXFalse", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
			if (lp.getLabel() == OUTPUT_FALSE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_FALSE);

		// sample training
		w.writeHeader("BOXTrue", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
			if (lp.getLabel() == OUTPUT_TRUE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_TRUE);

		w.close();
	}

	private static boolean printResultAndDetermineIfDone(NeuralNetworkHillClimbing nn, LabelledPointsAndBoxes trainingPoints, Neuron o1, int iteration)
	{
		StringBuilder fineLog = new StringBuilder();

		error = 0.0;
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
			error += nn.setInputs(lp.getX(), lp.getY()).forwardPropagation().setTargetOutputs(lp.getLabel()).getNetError(null);

		LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(error)) + "\n" + fineLog.toString());
		return error <= ERROR_MARGIN_NET;
	}
}
