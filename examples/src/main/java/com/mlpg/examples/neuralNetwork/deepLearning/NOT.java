package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.OutputCheck;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class NOT
{
	private NOT()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid());
		Neuron i1 = new InputNeuron(nn, "I1");

		/*  Testcase
		PerzeptronOutput o1 = new PerzeptronOutput(nn, "O1", 0.5);
		o1.connectInput(i1, -0.51);
		 */

		OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1").bias();
		o1.connectInput(i1);
		nn.computeLayers();
		nn.setSizeInputDataSet(2);

		int iteration = 0;
		double sumErrors;
		OutputCheck<Boolean> outputCheck;
		do
		{
			StringBuilder fineLog = new StringBuilder();
			iteration++;
			outputCheck = new OutputCheck<>();

			sumErrors = nn.train(nn.setInputs(INPUT_FALSE).forwardPropagation().setTargetOutputs(OUTPUT_TRUE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(o1.getFired(), true);

			sumErrors += nn.train(nn.setInputs(INPUT_TRUE).forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(o1.getFired(), false);

			LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(sumErrors)) + "\n" + fineLog.toString());
		} while (iteration < TRAININGCYCLES && (Math.abs(sumErrors) >= ERROR_MARGIN_NET || outputCheck.allNonEqualValues() > 0));

		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}
}
