package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.LabelledPoint;
import com.mlpg.methods.util.LabelledPointsAndBoxes;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.OutputCheck;
import com.mlpg.methods.util.csv.CSVWriter;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static com.mlpg.methods.neuralNetwork.Parameters.*;

public final class ThresholdX
{
	private ThresholdX()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid());
		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		/* manually trained for true & false
			OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1", -45);
			o1.connectInput(i1, 60);
		 */

		OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1").bias();
		o1.connectInput(i1);
		o1.connectInput(i2);

		OutputNeuronDeepLearning o2 = new OutputNeuronDeepLearning(nn, "O2").bias();
		o2.connectInput(i1);
		o2.connectInput(i2);

		nn.computeLayers();
		nn.setSizeInputDataSet(4);

		Set<LabelledPointsAndBoxes> trainingSet = new HashSet<>();

		LabelledPointsAndBoxes trainingPoints = new LabelledPointsAndBoxes();
		trainingPoints.addLabelledPoint(new LabelledPoint(0.5, 0.0, o1, null));
		trainingPoints.addLabelledPoint(new LabelledPoint(0.5, 0.0, o2, OUTPUT_TRUE));
		trainingSet.add(trainingPoints);

		trainingPoints = new LabelledPointsAndBoxes();
		trainingPoints.addLabelledPoint(new LabelledPoint(1.0, 0.0, o2, null));
		trainingPoints.addLabelledPoint(new LabelledPoint(1.0, 0.0, o1, OUTPUT_TRUE));

		trainingSet.add(trainingPoints);

		int iteration = 0;
		double netError;
		OutputCheck<Boolean> outputCheck;
		do
		{
			StringBuilder fineLog = new StringBuilder();
			outputCheck = new OutputCheck<>();

			netError = 0.0;
			Iterator<LabelledPointsAndBoxes> trainingSetIterator = trainingSet.iterator();
			while (trainingSetIterator.hasNext())
			{
				trainingPoints = trainingSetIterator.next();

				for (LabelledPoint lp : trainingPoints.getLabelledPoints())
				{
					netError += nn.setInputs(lp.getX(), lp.getY())
							.forwardPropagation().setTargetOutput(lp.getNeuron(), lp.getLabel()).getError(lp.getNeuron());

					outputCheck.addComputedAndToTrain(lp.getNeuron().getFired(),
							lp.getLabel() == null ? null : lp.getLabel() == OUTPUT_TRUE);
				}

				LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(netError)));
				// + "\n" + fineLog.toString());
				nn.train(netError);
			}

			iteration++;

		} while (iteration < TRAININGCYCLES && (Math.abs(netError) >= ERROR_MARGIN_NET || outputCheck.allNonEqualValues() > 0));

		writeResultsToFile(nn, trainingPoints, o1, "output_BOX.csv");
		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}

	private static void writeResultsToFile(
			NeuralNetworkDeepLearning nn, LabelledPointsAndBoxes trainingPoints, OutputNeuronDeepLearning o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:False values written: " + w.writeData(nn, o1, "NNFalse", false));
		LOGGER.info("NN:TRUE values written: " + w.writeData(nn, o1, "NNTrue", true));
		LOGGER.info("NN:UNKNOWN values written: " + w.writeData(nn, o1, "NNUnknown", null));

		// sample training
		w.writeHeader("BOXFalse", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
		{
			if (lp.getLabel() != null && lp.getLabel() == OUTPUT_FALSE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_FALSE);
		}

		w.writeHeader("BOXFalse", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
		{
			if (lp.getLabel() == null)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_UNKNOWN);
		}

		// sample training
		w.writeHeader("BOXTrue", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
		{
			if (lp.getLabel() != null && lp.getLabel() == OUTPUT_TRUE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_TRUE);
		}

		w.close();
	}
}
