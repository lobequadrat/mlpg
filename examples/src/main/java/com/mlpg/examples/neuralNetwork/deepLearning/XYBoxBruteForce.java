package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.Box;
import com.mlpg.methods.util.LabelledPointsAndBoxes;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static com.mlpg.methods.neuralNetwork.Parameters.LOGGER;
import static com.mlpg.methods.util.ManageNN.*;


public class XYBoxBruteForce implements Runnable
{
	private static int[] layersMaxNeuronsMax = new int[2];
	private static Semaphore semaphore = new Semaphore(1);
	private static boolean done;

	private int layersMax;
	private int neuronsMax;


	public XYBoxBruteForce(int layersMax, int neuronsMax)
	{
		this.layersMax = layersMax;
		this.neuronsMax = neuronsMax;
	}

	public static void main(String[] args) throws Exception
	{
		setup();

		// Runtime.getRuntime().availableProcessors()
		ExecutorService eService = Executors.newFixedThreadPool(4);
		layersMaxNeuronsMax[0] = 2;
		layersMaxNeuronsMax[1] = 1;

		boolean allJobsSubmitted = false;
		do
		{
			eService.submit(new XYBoxBruteForce(layersMaxNeuronsMax[0], layersMaxNeuronsMax[1]));
			allJobsSubmitted = increaseLayersMaxNeuronsMax(layersMaxNeuronsMax);
		}
		while (!allJobsSubmitted);

		eService.shutdown();
		if (!eService.awaitTermination(60000, TimeUnit.SECONDS))
			System.err.println("Threads didn't finish in 60000 seconds!");

		tearDown();
	}

	private static void setDone()
	{
		try
		{
			semaphore.acquire();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		XYBoxBruteForce.done = true;

		semaphore.release();
	}

	private static boolean isDone()
	{
		try
		{
			semaphore.acquire();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		boolean doneTmp = XYBoxBruteForce.done;
		semaphore.release();
		return doneTmp;
	}

	@Override
	public void run()
	{
		if (isDone())
			return;

		NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid());
		OutputNeuronDeepLearning outputNeuronToControl = new OutputNeuronDeepLearning(nn, "O1").bias();

		Set<Neuron> inputNeurons = new HashSet<>();
		inputNeurons.add(new InputNeuron(nn, "I1"));
		inputNeurons.add(new InputNeuron(nn, "I2"));

		List<Integer> numberOfNeuronsOfLayerX = determineNumberOfLayersAndNeurons(layersMax, neuronsMax, false);
		createAndConnectHiddenNeurons(nn, inputNeurons, numberOfNeuronsOfLayerX, outputNeuronToControl);

		Set<LabelledPointsAndBoxes> trainingSet = new HashSet<>();
		LabelledPointsAndBoxes.getBoxes().add(new Box(0.25, 0.25, 0.75, 0.75));
		LabelledPointsAndBoxes trainingPoints = new LabelledPointsAndBoxes(400, outputNeuronToControl, true);
		trainingSet.add(trainingPoints);

		double[] result = trainNN(nn, outputNeuronToControl, trainingSet, "output_BOX.csv", false);
		LOGGER.info("Layers: " + layersMax + ", Neurons: " + neuronsMax + ", NetError:" + result[1]);

		if (result[0] == 0)
		{
			LOGGER.info("Found solution : " + nn.toString());
			setDone();
		}
	}
}
