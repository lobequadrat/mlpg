package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.OutputCheck;
import com.mlpg.methods.util.csv.CSVWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class AND
{
	private AND()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid());
		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		// Testcase
		/*
		PerzeptronOutput o1 = new PerzeptronOutput(nn, "O1", -0.09);
		o1.connectInput(i1, 0.05);
		o1.connectInput(i2, 0.05);
		*/

		OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1").bias();
		o1.connectInput(i1);
		o1.connectInput(i2);
		nn.computeLayers();
		nn.setSizeInputDataSet(4);

		int iteration = 0;
		double error;
		OutputCheck<Boolean> outputCheck;
		do
		{
			StringBuilder fineLog = new StringBuilder();
			iteration++;
			outputCheck = new OutputCheck<>();

			error = nn.train(nn.setInputs(INPUT_FALSE, INPUT_FALSE).forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(o1.getFired(), false);
			error += nn.train(nn.setInputs(INPUT_TRUE, INPUT_FALSE).forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(o1.getFired(), false);
			error += nn.train(nn.setInputs(INPUT_FALSE, INPUT_TRUE).forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(o1.getFired(), false);
			error += nn.train(nn.setInputs(INPUT_TRUE, INPUT_TRUE).forwardPropagation().setTargetOutputs(OUTPUT_TRUE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(o1.getFired(), true);

			LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(error)) + "\n" + fineLog.toString());
		} while (iteration < TRAININGCYCLES && (Math.abs(error) >= ERROR_MARGIN_NET || outputCheck.allNonEqualValues() > 0));

		nn.setInputs(INPUT_FALSE, INPUT_TRUE).forwardPropagation();

		writeResultsToFile(nn, o1, "output_AND2.csv");
		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}

	private static void writeResultsToFile(NeuralNetwork nn, Neuron o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:False values written: " + w.writeData(nn, o1, "NNFalse", false));
		LOGGER.info("NN:True values written: " + w.writeData(nn, o1, "NNTrue", true));

		// sample training
		w.writeHeader("ANDFalse", "input1", "input2", "output");
		w.writeLine(INPUT_FALSE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_TRUE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_FALSE, INPUT_TRUE, OUTPUT_FALSE);
		w.writeHeader("ANDTrue", "input1", "input2", "output");
		w.writeLine(INPUT_TRUE, INPUT_TRUE, OUTPUT_FALSE);
		w.close();
	}
}
