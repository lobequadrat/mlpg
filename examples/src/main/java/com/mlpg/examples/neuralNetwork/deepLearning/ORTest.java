package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.csv.CSVWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class ORTest
{
	private ORTest()
	{
	}

	public static void main(String[] args)
	{
		ManageNN.setup();

		NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid());
		Neuron i1 = new InputNeuron(nn, "I1");
		Neuron i2 = new InputNeuron(nn, "I2");

		OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1").bias(0.3);
		o1.connectInput(i1, 0.1);
		o1.connectInput(i2, 0.2);
		nn.computeLayers();
		nn.setSizeInputDataSet(4);

		StringBuilder fineLog = new StringBuilder();

		double errorSum = nn.setInputs(INPUT_FALSE, INPUT_FALSE).setTargetOutputs(OUTPUT_FALSE).getNetError(null);
		LOGGER.info("o1 : " + o1.computeOutput());

		errorSum += nn.setInputs(INPUT_FALSE, INPUT_TRUE).setTargetOutputs(OUTPUT_TRUE).getNetError(null);
		LOGGER.info("o1 : " + o1.computeOutput());

		errorSum += nn.setInputs(INPUT_TRUE, INPUT_FALSE).setTargetOutputs(OUTPUT_TRUE).getNetError(null);
		LOGGER.info("o1 : " + o1.computeOutput());

		errorSum += nn.setInputs(INPUT_TRUE, INPUT_TRUE).setTargetOutputs(OUTPUT_TRUE).getNetError(null);
		LOGGER.info("o1 : " + o1.computeOutput());

		LOGGER.info("Error sum : " + errorSum * errorSum * 0.5);

		nn.train(nn.setInputs(INPUT_FALSE, INPUT_FALSE).setTargetOutputs(OUTPUT_FALSE).getNetError(null));
		nn.train(nn.setInputs(INPUT_TRUE, INPUT_FALSE).setTargetOutputs(OUTPUT_TRUE).getNetError(null));
		nn.train(nn.setInputs(INPUT_FALSE, INPUT_TRUE).setTargetOutputs(OUTPUT_TRUE).getNetError(null));
		nn.train(nn.setInputs(INPUT_TRUE, INPUT_TRUE).setTargetOutputs(OUTPUT_TRUE).getNetError(null));

		// --- small error ---

		LOGGER.info("w1 : " + nn.getInputSynapses().get(o1).get(i1));
		LOGGER.info("w2 : " + nn.getInputSynapses().get(o1).get(i2));

		// sumErrors += nn.train(nn.setInputs(INPUT_FALSE, INPUT_TRUE).simulate().setTargetOutputs(OUTPUT_TRUE).getError(fineLog));
		// LOGGER.info(nn.toString().concat(", Error: ").concat(getDfl().format(sumErrors)) + "\n" + fineLog.toString());

		// writeResultsToFile(nn, o1, "output_OR.csv");
		// LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}

	private static void writeResultsToFile(NeuralNetwork nn, Neuron o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:False values written: " + w.writeData(nn, o1, "NNFalse", false));
		LOGGER.info("NN:True values written: " + w.writeData(nn, o1, "NNTrue", true));

		// sample training
		w.writeHeader("ANDFalse", "input1", "input2", "output");
		w.writeLine(INPUT_FALSE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_TRUE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_FALSE, INPUT_TRUE, OUTPUT_FALSE);
		w.writeHeader("ANDTrue", "input1", "input2", "output");
		w.writeLine(INPUT_TRUE, INPUT_TRUE, OUTPUT_FALSE);
		w.close();
	}
}
