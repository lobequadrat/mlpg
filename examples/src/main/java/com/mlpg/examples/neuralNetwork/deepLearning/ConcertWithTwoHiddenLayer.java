
package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.deeplearning.*;
import com.mlpg.methods.util.ManageNN;
import com.mlpg.methods.util.OutputCheck;
import com.mlpg.methods.util.csv.CSVWriter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class ConcertWithTwoHiddenLayer
{
	private ConcertWithTwoHiddenLayer()
	{
	}

	public static void main(String[] args)
	{

		ManageNN.setup();

		NeuralNetworkDeepLearning nn = concertTwoHiddenLayer();

		Neuron out = nn.getOutputNeurons().get(0);

		nn.setSizeInputDataSet(8);

		int iteration = 0;
		double sumErrors;
		OutputCheck<Boolean> outputCheck;
		do
		{
			StringBuilder fineLog = new StringBuilder();
			iteration++;
			outputCheck = new OutputCheck<>();

			sumErrors = nn.train(nn.setInputs(INPUT_FALSE, INPUT_FALSE, INPUT_FALSE)
					.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(out.getFired(), false);
			sumErrors += nn.train(nn.setInputs(INPUT_FALSE, INPUT_FALSE, INPUT_TRUE)
					.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(out.getFired(), false);
			sumErrors += nn.train(nn.setInputs(INPUT_FALSE, INPUT_TRUE, INPUT_FALSE)
					.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(out.getFired(), false);
			sumErrors += nn.train(nn.setInputs(INPUT_TRUE, INPUT_FALSE, INPUT_FALSE)
					.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(out.getFired(), false);
			sumErrors += nn.train(nn.setInputs(INPUT_TRUE, INPUT_FALSE, INPUT_TRUE)
					.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(out.getFired(), false);
			sumErrors += nn.train(nn.setInputs(INPUT_TRUE, INPUT_TRUE, INPUT_FALSE)
					.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(out.getFired(), false);
			sumErrors += nn.train(nn.setInputs(INPUT_FALSE, INPUT_TRUE, INPUT_TRUE)
					.forwardPropagation().setTargetOutputs(OUTPUT_FALSE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(out.getFired(), false);
			sumErrors += nn.train(nn.setInputs(INPUT_TRUE, INPUT_TRUE, INPUT_TRUE)
					.forwardPropagation().setTargetOutputs(OUTPUT_TRUE).getNetError(fineLog));
			outputCheck.addComputedAndToTrain(out.getFired(), true);

			LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ").concat(getDfl().format(sumErrors)) + "\n" + fineLog.toString());
		} while (iteration < TRAININGCYCLES && (Math.abs(sumErrors) >= ERROR_MARGIN_NET || outputCheck.allNonEqualValues() > 0));

		// writeResultsToFile(nn, n6, "output_CONCERT.csv");
		LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		ManageNN.tearDown();
	}

	private static NeuralNetworkDeepLearning concertTwoHiddenLayer()
	{

		NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid());
		Neuron n1 = new InputNeuron(nn, "I1", INPUT_TRUE);
		Neuron n2 = new InputNeuron(nn, "I2", INPUT_TRUE);
		Neuron n3 = new InputNeuron(nn, "I3", INPUT_TRUE);

		Neuron n4 = new HiddenNeuronDeepLearning(nn, "H4").bias(0.2);
		Neuron n5 = new HiddenNeuronDeepLearning(nn, "H5").bias(0.2);

		Neuron n6 = new HiddenNeuronDeepLearning(nn, "H5").bias(0.2).activationFunction(new RELU());
		Neuron n7 = new HiddenNeuronDeepLearning(nn, "H6").bias(0.2).activationFunction(new RELU());

		Neuron o = new OutputNeuronDeepLearning(nn, "O").bias(0.3);

		n4.connectInput(n1, 0.3);
		n4.connectInput(n2, 0.4);
		n4.connectInput(n3, -0.7);

		n5.connectInput(n1, -0.8);
		n5.connectInput(n2, -0.5);
		n5.connectInput(n3, 0.7);

		n6.connectInput(n4, 0.3);
		n6.connectInput(n5, 0.4);

		n7.connectInput(n4, -0.5);
		n7.connectInput(n5, 0.7);

		o.connectInput(n6, 0.4);
		o.connectInput(n7, 0.6);

		nn.computeLayers();
		return nn;
	}

	private static void writeResultsToFile(NeuralNetwork nn, Neuron o1, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// sample training
		w.writeHeader("CONCERTFalse", "input1", "input2", "output");
		w.writeLine(INPUT_FALSE, INPUT_FALSE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_FALSE, INPUT_FALSE, INPUT_TRUE, OUTPUT_FALSE);
		w.writeLine(INPUT_FALSE, INPUT_TRUE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_TRUE, INPUT_FALSE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_TRUE, INPUT_FALSE, INPUT_TRUE, OUTPUT_FALSE);
		w.writeLine(INPUT_TRUE, INPUT_TRUE, INPUT_FALSE, OUTPUT_FALSE);
		w.writeLine(INPUT_FALSE, INPUT_TRUE, INPUT_TRUE, OUTPUT_FALSE);
		w.writeLine(INPUT_TRUE, INPUT_TRUE, INPUT_TRUE, OUTPUT_TRUE);

		w.writeHeader("CONCERTTrue", "input1", "input2", "output");
		w.writeLine(INPUT_TRUE, INPUT_TRUE, OUTPUT_FALSE);
		w.close();
	}
}
