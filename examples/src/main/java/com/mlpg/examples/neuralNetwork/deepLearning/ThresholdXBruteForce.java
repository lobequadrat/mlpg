package com.mlpg.examples.neuralNetwork.deepLearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.LabelledPoint;
import com.mlpg.methods.util.LabelledPointsAndBoxes;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.mlpg.methods.neuralNetwork.Parameters.OUTPUT_FALSE;
import static com.mlpg.methods.neuralNetwork.Parameters.OUTPUT_TRUE;
import static com.mlpg.methods.util.ManageNN.*;

public final class ThresholdXBruteForce
{
	private ThresholdXBruteForce()
	{
	}

	public static void main(String[] args)
	{
		setup();

		boolean done;
		int[] layersMaxNeuronsMax = new int[2];
		layersMaxNeuronsMax[0] = 1;
		layersMaxNeuronsMax[1] = 1;
		do
		{
			NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid());
			OutputNeuronDeepLearning outputNeuronToControl = new OutputNeuronDeepLearning(nn, "O1").bias();

			Set<Neuron> inputNeurons = new HashSet<>();
			inputNeurons.add(new InputNeuron(nn, "I1"));
			inputNeurons.add(new InputNeuron(nn, "I2"));

			List<Integer> numberOfNeuronsOfLayerX = determineNumberOfLayersAndNeurons(layersMaxNeuronsMax[0], layersMaxNeuronsMax[1], false);
			createAndConnectHiddenNeurons(nn, inputNeurons, numberOfNeuronsOfLayerX, outputNeuronToControl);

			Set<LabelledPointsAndBoxes> trainingSet = setTrainingParameters(outputNeuronToControl);

			done = trainNN(nn, outputNeuronToControl, trainingSet, "output_BOX.csv", true)[0] > 0
					|| increaseLayersMaxNeuronsMax(layersMaxNeuronsMax);
		}
		while (!done);

		tearDown();
	}


	private static Set<LabelledPointsAndBoxes> setTrainingParameters(OutputNeuronDeepLearning outputNeuronToControl)
	{
		Set<LabelledPointsAndBoxes> trainingSet = new HashSet<>();
		LabelledPointsAndBoxes trainingPoints = new LabelledPointsAndBoxes();
		trainingPoints.addLabelledPoint(new LabelledPoint(0.49, 0.0, outputNeuronToControl, OUTPUT_FALSE));
		//trainingPoints.addLabelledPoint(new LabelledPoint(0.4, 0.0, o1, OUTPUT_FALSE));
		trainingSet.add(trainingPoints);

		trainingPoints = new LabelledPointsAndBoxes();
		trainingPoints.addLabelledPoint(new LabelledPoint(0.51, 0.0, outputNeuronToControl, OUTPUT_TRUE));
		//trainingPoints.addLabelledPoint(new LabelledPoint(0.6, 0.0, o1, OUTPUT_TRUE));
		trainingSet.add(trainingPoints);
		return trainingSet;
	}
}
