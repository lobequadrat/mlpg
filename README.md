# Project Purpose

This project is a(n implementation) playground to investigate and understand machine learning algorithmus from scratch.

# Implementation status

- Neural Network (NN) Core API:
	- Topology definition, i.e., input nodes, hidden nodes, output nodes and connections
	- Simulation (fired or not) & training
	- Visualization
		- Output CSV + Scatterplot
		- Topology in 2D (https://graphstream-project.org/)
	- XML serialization
	- Neuron Variants:
		- Perzeptrons Network: https://en.wikipedia.org/wiki/Perceptron
			- discrete switching behavior
			- training with Hill Climbing
		- Deep Neural Networks with
			- Activation Functions (Sigmoid)
			- training with Backpropagation
	- Neural Network Classes:
		- Feedforward NN


- Examples:
	- Boolean Algebra
		- AND
		- OR
		- NOT
		- XOR
	- Concert visit
	- Threshold
	- Box / Area

	
# get started

[onboarding wiki](https://gitlab.com/lobequadrat/mlpg/-/wikis/Onboarding)

# License

Eclipse Public License 2.0


# Contributors

- Ralf Buschermöhle (buschermoehle@protonmail.com)
- Daniel Süpke (daniel@suepke.net)

Contributers welcome - please contact via email.
