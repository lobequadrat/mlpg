package com.mlpg.methods.neuralNetwork.hillClimbing;

import com.mlpg.methods.neuralNetwork.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/** Implementation of a hidden neuron following the hill climbing method. */
public class HiddenNeuronHillClimbing extends Neuron
{
	/**
	 * Stores a copy in case the training needs to be rolled back.
	 */
	private HashMap<Neuron, Double> inputs2WeightsCopy = new HashMap<>();
	private final Double biasCopy = null;


	public HiddenNeuronHillClimbing(NeuralNetwork nn, String name)
	{
		super(nn, name);
		addToNeuronSets(true);
	}


	protected HiddenNeuronHillClimbing(NeuralNetwork nn, String name, boolean addToHiddenNeurons)
	{
		super(nn, name);
		addToNeuronSets(addToHiddenNeurons);
	}

	protected void addToNeuronSets(boolean addToHiddenNeurons)
	{
		if (addToHiddenNeurons)
			getNn().getHiddenNeurons().add(this);

		getNn().getHiddenOutputNeurons().add(this);
	}

	public HiddenNeuronHillClimbing bias()
	{
		return bias(null);
	}

	public HiddenNeuronHillClimbing bias(Double bias)
	{
		if (bias == null)
			return bias((Parameters.RANDOM.nextBoolean() ? -1 : 1) * Parameters.RANDOM.nextDouble());
		setBiasNeuron(new BiasNeuron(getNn(), "bias"));
		connectInput(getBiasNeuron(), bias);
		return this;
	}

	/**
	 * Calculates the output of the hidden neuron.
	 * During calculation checks whether input is coming from input neuron, because input node values are required.
	 *
	 * @return output, which can be restricted
	 */
	@Override
	public Double computeOutput()
	{
		setOutput(0.0);

		Map<Neuron, Double> inputNeurons2Weights = getNn().getInputSynapses().get(this);

		for (Neuron input : inputNeurons2Weights.keySet())
			if (input.getFired())
				setOutput(getOutput() + (input instanceof InputNeuron ? input.computeOutput()
						* inputNeurons2Weights.get(input) : inputNeurons2Weights.get(input)));


		setOutput(restrictValue(getOutput()));
		return getOutput();
	}

	/**
	 * Before changing weights a copy is stored.
	 * Weights get changed in a random direction.
	 * The change is determined using a random value and a preset lambda.
	 */
	@Override
	public void train()
	{
		Map<Neuron, Double> inputs2Weights = getNn().getInputSynapses().get(this);
		inputs2WeightsCopy = new LinkedHashMap<>(inputs2Weights);
		inputs2Weights.clear();

		for (Neuron input : inputs2WeightsCopy.keySet())
		{

			double oldWeight = inputs2WeightsCopy.get(input);
			double change = Parameters.RANDOM.nextDouble() * Parameters.LAMBDA;

			double newWeight = restrictValue(oldWeight + (Parameters.RANDOM.nextBoolean() ? change : -change));
			inputs2Weights.put(input, newWeight);
		}
	}

	/**
	 * When the new error is larger than the previous error, training data is reset to previous state.
	 * Check concerning the error is executed in {@link NeuralNetworkHillClimbing}
	 */
	public void undoTraining()
	{
		Map<Neuron, Double> inputNeurons2Weights = new LinkedHashMap<>(inputs2WeightsCopy);
		getNn().getInputSynapses().put(this, inputNeurons2Weights);
		inputs2WeightsCopy.clear();
	}
}
