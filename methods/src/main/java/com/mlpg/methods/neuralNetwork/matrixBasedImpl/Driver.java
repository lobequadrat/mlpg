package com.mlpg.methods.neuralNetwork.matrixBasedImpl;

import java.util.List;

public final class Driver
{

	private Driver()
	{
	}

	private static double[][] trainingSetData = {
			{0, 0, 0},
			{0, 0, 1},
			{0, 1, 0},
			{0, 1, 1},
			{1, 0, 0},
			{1, 0, 1},
			{1, 1, 0},
			{1, 1, 1},
	};

	private static double[][] trainingSetLabels = {
			{0}, {0}, {0}, {0}, {0}, {0}, {0}, {1}
	};


	public static void main(String[] args)
	{

		// Concert, test cases: -> we will write unit tests
//		NeuralNetworkOneHiddenLayer nn = new NeuralNetworkOneHiddenLayer(3,2,1);
//		NeuralNetworkTwoHiddenLayers nn = new NeuralNetworkTwoHiddenLayers(3,2,2,1);
//		NeuralNetworkOneHiddenLayer nn = new NeuralNetworkOneHiddenLayer(3,10,1);
//		NeuralNetworkMultipleHiddenLayers nn = new NeuralNetworkMultipleHiddenLayers(3,2,1);
//		NeuralNetworkMultipleHiddenLayers nn = new NeuralNetworkMultipleHiddenLayers(3,10,10,20,1);
//		NeuralNetworkMultipleHiddenLayers nn = new NeuralNetworkMultipleHiddenLayers(3,2,2,2,2,1);
//		NeuralNetworkMultipleHiddenLayers nn = new NeuralNetworkMultipleHiddenLayers(3,2,1);
//		NeuralNetworkMultipleHiddenLayers nn = new NeuralNetworkMultipleHiddenLayers(3,2,3,3,2,1);
		final int numberInputNeurons = 3;
		final int numberNeuronsLayer1 = 5;
		final int numberNeuronslayer2 = 2;
		final int numberOutputNeurons = 1;
		NeuralNetworkMultipleHiddenLayers nn =
				new NeuralNetworkMultipleHiddenLayers(numberInputNeurons, numberNeuronsLayer1, numberNeuronslayer2, numberOutputNeurons);

		List<Double> output;

		final int epochs = 250000;
		nn.fit(trainingSetData, trainingSetLabels, epochs);
		double[][] input = {
				{0, 0, 0},
				{0, 0, 1},
				{0, 1, 0},
				{0, 1, 1},
				{1, 0, 0},
				{1, 0, 1},
				{1, 1, 0},
				{1, 1, 1},
		};
		for (double[] d : input)
		{
			output = nn.predict(d);
			System.out.println(output.toString());
		}

	}

}
