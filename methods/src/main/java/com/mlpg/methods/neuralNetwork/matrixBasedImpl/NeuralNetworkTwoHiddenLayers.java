package com.mlpg.methods.neuralNetwork.matrixBasedImpl;

import java.util.List;
import java.util.Random;

public class NeuralNetworkTwoHiddenLayers
{

    private final double lRate = 0.01;
    private Matrix weightsIh1;
    private Matrix weightsH1H2;
    private Matrix weightsH2O;
    private Matrix biasH1;
    private Matrix biasH2;
    private Matrix biasO;

    public NeuralNetworkTwoHiddenLayers(int input, int hidden1, int hidden2, int out)
    {
        weightsIh1 = new Matrix(hidden1, input);
        weightsH1H2 = new Matrix(hidden2, hidden1);
        weightsH2O = new Matrix(out, hidden2);

        biasH1 = new Matrix(hidden1, 1);
        biasH2 = new Matrix(hidden2, 1);
        biasO = new Matrix(out, 1);

    }

    public List<Double> predict(double[] inputSeq)
    {
        Matrix input = Matrix.fromArray(inputSeq);
        Matrix hidden1 = Matrix.multiply(weightsIh1, input);
        hidden1.add(biasH1);
        hidden1.sigmoid();

        Matrix hidden2 = Matrix.multiply(weightsH1H2, hidden1);
        hidden2.add(biasH2);
        hidden2.sigmoid();

        Matrix output = Matrix.multiply(weightsH2O, hidden2);
        output.add(biasO);
        output.sigmoid();

        return output.toArray();
    }


    public void fit(double[][] sampleIn, double[][] sampleOut, int epochs)
    {
        Random rand = new Random(1);
        for (int i = 0; i < epochs; i++)
        {
            int sampleN = (int) (rand.nextDouble() * sampleIn.length);
            this.train(sampleIn[sampleN], sampleOut[sampleN]);
        }
    }

    public void train(double[] sampleIn, double[] sampleOut)
    {
        // forwardprogagation
        Matrix input = Matrix.fromArray(sampleIn);
        Matrix hidden1 = Matrix.multiply(weightsIh1, input);
        hidden1.add(biasH1);
        hidden1.sigmoid();

        Matrix hidden2 = Matrix.multiply(weightsH1H2, hidden1);
        hidden2.add(biasH2);
        hidden2.sigmoid();

        Matrix output = Matrix.multiply(weightsH2O, hidden2);
        output.add(biasO);
        output.sigmoid();

        // backpropagation
        Matrix target = Matrix.fromArray(sampleOut);

        Matrix error = Matrix.subtract(target, output);

        Matrix gradient = output.dsigmoid();
        gradient.multiply(error);
        gradient.multiply(lRate);

        Matrix hidden2T = Matrix.transpose(hidden2);
        Matrix wh2ODelta = Matrix.multiply(gradient, hidden2T);

        weightsH2O.add(wh2ODelta);
        biasO.add(gradient);

        //-----------------------------

        Matrix wh2OT = Matrix.transpose(weightsH2O);
        Matrix hidden2Errors = Matrix.multiply(wh2OT, error);

        Matrix h2Gradient = hidden2.dsigmoid();
        h2Gradient.multiply(hidden2Errors);
        h2Gradient.multiply(lRate);

        Matrix h1T = Matrix.transpose(hidden1);
        Matrix wh1H2Delta = Matrix.multiply(h2Gradient, h1T);

        weightsH1H2.add(wh1H2Delta);
        biasH2.add(h2Gradient);

        //-----------------------------

        Matrix wh1H2T = Matrix.transpose(weightsH1H2);
        Matrix hidden1Errors = Matrix.multiply(wh1H2T, hidden2Errors);

        Matrix h1Gradient = hidden1.dsigmoid();
        h1Gradient.multiply(hidden1Errors);
        h1Gradient.multiply(lRate);

        Matrix iT = Matrix.transpose(input);
        Matrix wih1Delta = Matrix.multiply(h1Gradient, iT);

        weightsIh1.add(wih1Delta);
        biasH1.add(h1Gradient);

    }

}
