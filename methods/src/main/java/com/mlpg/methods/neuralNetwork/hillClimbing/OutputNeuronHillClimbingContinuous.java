package com.mlpg.methods.neuralNetwork.hillClimbing;


import com.mlpg.methods.neuralNetwork.BiasNeuron;
import com.mlpg.methods.neuralNetwork.Parameters;

/** Represents a specialization of the class OutputNeuronHillClimbing. */
public class OutputNeuronHillClimbingContinuous extends HiddenNeuronHillClimbingContinuous
{
	public OutputNeuronHillClimbingContinuous(NeuralNetworkHillClimbing nn, String name)
	{
		super(nn, name, false);
		nn.getOutputNeurons().add(this);
	}

	public OutputNeuronHillClimbingContinuous bias()
	{
		return bias(null);
	}

	public OutputNeuronHillClimbingContinuous bias(Double bias)
	{
		if (bias == null)
			return bias((Parameters.RANDOM.nextBoolean() ? -1 : 1) * Parameters.RANDOM.nextDouble());
		setBiasNeuron(new BiasNeuron(getNn(), "bias"));
		connectInput(getBiasNeuron(), bias);
		return this;
	}
}
