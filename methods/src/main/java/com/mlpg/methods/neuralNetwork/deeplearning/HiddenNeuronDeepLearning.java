package com.mlpg.methods.neuralNetwork.deeplearning;

import com.mlpg.methods.neuralNetwork.BiasNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.Parameters;
import com.mlpg.methods.neuralNetwork.hillClimbing.HiddenNeuronHillClimbing;

import java.util.Map;

import static com.mlpg.methods.neuralNetwork.Parameters.OUTPUT_FALSE;
import static com.mlpg.methods.neuralNetwork.Parameters.OUTPUT_TRUE;

/**
 * Deep Learning for hidden neurons.
 */
public class HiddenNeuronDeepLearning extends HiddenNeuronHillClimbing implements NeuronDeepLearning
{
	private transient NeuralNetworkDeepLearning dlNn; // Variables are not polymorphic in Java

	private ActivationFunction activationFunction;

	public HiddenNeuronDeepLearning(NeuralNetworkDeepLearning nn, String name)
	{
		this(nn, name, true);
	}

	public HiddenNeuronDeepLearning(NeuralNetworkDeepLearning nn, String name,
									boolean addToHiddenNeurons)
	{
		super(nn, name, addToHiddenNeurons);
		this.dlNn = nn;
	}

	@Override
	public HiddenNeuronDeepLearning activationFunction(ActivationFunction activationFunctionOfNeuron)
	{
		activationFunction = activationFunctionOfNeuron;
		return this;
	}

	public HiddenNeuronDeepLearning bias()
	{
		return bias(null);
	}

	public HiddenNeuronDeepLearning bias(Double bias)
	{
		if (bias == null)
			return bias((Parameters.RANDOM.nextBoolean() ? -1 : 1) * Parameters.RANDOM.nextDouble());
		setBiasNeuron(new BiasNeuron(getNn(), "bias"));
		connectInput(getBiasNeuron(), bias);
		return this;
	}

	/**
	 * Forward propagation. Fires neuron if error margin from current node is low enough.
	 */
	@Override
	public void forwardPropagation()
	{
		Double errorMarginNode = dlNn.getErrorMarginNode();
		setOutput(computeOutput());

		if (getOutput() >= OUTPUT_FALSE && getOutput() <= OUTPUT_FALSE + errorMarginNode)
			setFired(false);
		else if (getOutput() >= OUTPUT_TRUE - errorMarginNode && getOutput() <= OUTPUT_TRUE)
			setFired(true);
		else
			setFired(null);
	}

	/**
	 * Calculates output of a neuron with all attached neurons from product of output and weights.
	 */
	@Override
	public Double computeOutput()
	{
		setOutput(0.0);
		Map<Neuron, Double> inputNeurons2Weights = getNn().getInputSynapses().get(this);

		for (Neuron input : inputNeurons2Weights.keySet())
			setOutput(getOutput() + input.getOutput() * inputNeurons2Weights.get(input));

		setOutput(activationFunction != null
				? activationFunction.compute(getOutput())
				: dlNn.getDefaultActivationFunction() != null
				? dlNn.getDefaultActivationFunction().compute(getOutput())
				: getOutput());

		return getOutput();
	}

	public void connectAndOrChangeWeight(Neuron inputNeuron, Double weight)
	{
		// not restricting the weight any more
		getNn().addOrChangeSynapse(inputNeuron, this, weight);
	}

	@Override
	public ActivationFunction getActivationFunction()
	{
		return activationFunction;
	}
}
