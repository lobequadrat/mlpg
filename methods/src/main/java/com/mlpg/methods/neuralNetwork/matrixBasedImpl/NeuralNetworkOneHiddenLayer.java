package com.mlpg.methods.neuralNetwork.matrixBasedImpl;

import java.util.List;

public class NeuralNetworkOneHiddenLayer
{

    private Matrix weightsIh;
    private Matrix weightsHo;
    private Matrix biasH;
    private Matrix biasO;
    private final double lRate = 0.01;

    public NeuralNetworkOneHiddenLayer(int input, int hidden, int out)
    {
        weightsIh = new Matrix(hidden, input);
        weightsHo = new Matrix(out, hidden);

        biasH = new Matrix(hidden, 1);
        biasO = new Matrix(out, 1);

    }

    public List<Double> predict(double[] inputSeq)
    {
        Matrix input = Matrix.fromArray(inputSeq);
        Matrix hidden = Matrix.multiply(weightsIh, input);
        hidden.add(biasH);
        hidden.sigmoid();

        Matrix output = Matrix.multiply(weightsHo, hidden);
        output.add(biasO);
        output.sigmoid();

        return output.toArray();
    }


    public void fit(double[][] sampleIn, double[][] sampleOut, int epochs)
    {
        for (int i = 0; i < epochs; i++)
        {
            int sampleN = (int) (Math.random() * sampleIn.length);
            this.train(sampleIn[sampleN], sampleOut[sampleN]);
        }
    }

    public void train(double[] sampleIn, double[] sampleOut)
    {
        Matrix input = Matrix.fromArray(sampleIn);
        Matrix hidden = Matrix.multiply(weightsIh, input);
        hidden.add(biasH);
        hidden.sigmoid();

        Matrix output = Matrix.multiply(weightsHo, hidden);
        output.add(biasO);
        output.sigmoid();

        Matrix target = Matrix.fromArray(sampleOut);

        Matrix error = Matrix.subtract(target, output);
        Matrix gradient = output.dsigmoid();
        gradient.multiply(error);
        gradient.multiply(lRate);

        Matrix hiddenT = Matrix.transpose(hidden);
        Matrix whoDelta = Matrix.multiply(gradient, hiddenT);

        weightsHo.add(whoDelta);
        biasO.add(gradient);
        //--------------------------
        Matrix whoT = Matrix.transpose(weightsHo);
        Matrix hiddenErrors = Matrix.multiply(whoT, error);

        Matrix hGradient = hidden.dsigmoid();
        hGradient.multiply(hiddenErrors);
        hGradient.multiply(lRate);

        Matrix iT = Matrix.transpose(input);
        Matrix wihDelta = Matrix.multiply(hGradient, iT);

        weightsIh.add(wihDelta);
        biasH.add(hGradient);

    }


}
