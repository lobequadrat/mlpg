package com.mlpg.methods.neuralNetwork.hillClimbing;

import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import com.mlpg.methods.neuralNetwork.Parameters;
import lombok.Getter;

import java.util.ArrayList;

/** Implementation of a neural network using the hill climbing method. */
@Getter
public class NeuralNetworkHillClimbing extends NeuralNetwork
{
	private final ArrayList<Neuron> neuronsTrained = new ArrayList<>();
	private Double previousError;

	/**
	 * Training the neural network with hill climbing.
	 * Randomized changing of weights using train method in {@link HiddenNeuronHillClimbing}.
	 * Training gets undone if resulting error is larger than previous error.
	 *
	 * @param error
	 *
	 * @return new error after weight changes
	 */
	public double train(double error)
	{
		if (previousError == null || error <= previousError)
		{
			neuronsTrained.clear();

			for (Neuron neuron : getHiddenOutputNeurons())
			{
				if (Parameters.RANDOM.nextDouble() < Parameters.HILL_CLIMBING_NEURON_TRAINING_PROBABILITY)
				{
					neuron.train();
					neuronsTrained.add(neuron);
				}
			}

			previousError = error;
		}
		else
		{
			undoTraining();
		}

		return error;
	}

	/**
	 * If new error is larger than previous error the training is rolled back.
	 */
	public void undoTraining()
	{
		Parameters.LOGGER.info("UNDO Training");

		for (Neuron neuron : neuronsTrained)
			neuron.undoTraining();
	}
}
