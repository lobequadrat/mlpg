package com.mlpg.methods.neuralNetwork.deeplearning;

/**
 * Activation Function with the Tanh method.
 */
public class Tanh extends ActivationFunction
{
	@Override
	public double compute(double value)
	{
		return (Math.exp(value) - Math.exp(-value)) / (Math.exp(value) + Math.exp(-value));
	}

	@Override
	protected double computeDerivationOfNode(double value)
	{
		return 1 - Math.pow(compute(value), 2);
	}
}
