package com.mlpg.methods.neuralNetwork.deeplearning;

import com.mlpg.methods.neuralNetwork.BiasNeuron;
import com.mlpg.methods.neuralNetwork.Parameters;

/**
 * Output neuron implementation.
 */
public class OutputNeuronDeepLearning extends HiddenNeuronDeepLearning implements NeuronDeepLearning
{

	/**
	 * Creates Output Neuron with given bias.
	 *
	 * @param name
	 * @param nn
	 */
	public OutputNeuronDeepLearning(NeuralNetworkDeepLearning nn, String name)
	{
		super(nn, name, false);
		nn.getOutputNeurons().add(this);
	}

	public OutputNeuronDeepLearning bias()
	{
		return bias(null);
	}

	public OutputNeuronDeepLearning bias(Double bias)
	{
		if (bias == null)
			return bias((Parameters.RANDOM.nextBoolean() ? -1 : 1) * Parameters.RANDOM.nextDouble());
		setBiasNeuron(new BiasNeuron(getNn(), "bias"));
		connectInput(getBiasNeuron(), bias);
		return this;
	}
}
