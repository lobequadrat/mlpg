package com.mlpg.methods.neuralNetwork.hillClimbing;


import com.mlpg.methods.neuralNetwork.BiasNeuron;
import com.mlpg.methods.neuralNetwork.Parameters;

/** Implementation of a output neuron. */
public class OutputNeuronHillClimbing extends HiddenNeuronHillClimbing
{
	public OutputNeuronHillClimbing(NeuralNetworkHillClimbing nn, String name)
	{
		super(nn, name, false);
		nn.getOutputNeurons().add(this);
	}

	public OutputNeuronHillClimbing bias()
	{
		return bias(null);
	}

	public OutputNeuronHillClimbing bias(Double bias)
	{
		if (bias == null)
			return bias((Parameters.RANDOM.nextBoolean() ? -1 : 1) * Parameters.RANDOM.nextDouble());
		setBiasNeuron(new BiasNeuron(getNn(), "bias"));
		connectInput(getBiasNeuron(), bias);
		return this;
	}
}
