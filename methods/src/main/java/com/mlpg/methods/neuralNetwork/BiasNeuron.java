package com.mlpg.methods.neuralNetwork;

/**
 * Biases are in itself implemented as a neuron.
 * Can be connected as any other neuron.
 */
public class BiasNeuron extends Neuron
{
	public BiasNeuron(NeuralNetwork nn, String name)
	{
		super(nn, name);
	}

	@Override
	public void train()
	{
		// nothing to see here
	}

	@Override
	public void undoTraining()
	{
		// nothing to see here
	}

	@Override
	public Double computeOutput()
	{
		return 1.0;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
