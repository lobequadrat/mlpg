package com.mlpg.methods.neuralNetwork.deeplearning;

import com.mlpg.methods.neuralNetwork.Neuron;

public interface NeuronDeepLearning
{
	ActivationFunction getActivationFunction();

	Neuron activationFunction(ActivationFunction activationFunction);
}
