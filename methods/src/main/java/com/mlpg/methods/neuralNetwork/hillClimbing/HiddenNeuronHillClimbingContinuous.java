package com.mlpg.methods.neuralNetwork.hillClimbing;

import com.mlpg.methods.neuralNetwork.BiasNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.Parameters;

import java.util.Map;

/** Represents a specialization of the class HiddenNeuronHillClimbing. */
public class HiddenNeuronHillClimbingContinuous extends HiddenNeuronHillClimbing
{

	public HiddenNeuronHillClimbingContinuous(NeuralNetworkHillClimbing nn, String name)
	{
		this(nn, name, true);
	}

	public HiddenNeuronHillClimbingContinuous(NeuralNetworkHillClimbing nn, String name,
											  boolean addToHiddenNeurons)
	{
		super(nn, name, addToHiddenNeurons);
	}

	public HiddenNeuronHillClimbingContinuous bias()
	{
		return bias(null);
	}

	public HiddenNeuronHillClimbingContinuous bias(Double bias)
	{
		if (bias == null)
			return bias((Parameters.RANDOM.nextBoolean() ? -1 : 1) * Parameters.RANDOM.nextDouble());
		setBiasNeuron(new BiasNeuron(getNn(), "bias"));
		connectInput(getBiasNeuron(), bias);
		return this;
	}

	@Override
	public Double computeOutput()
	{
		setOutput(0.0);
		Map<Neuron, Double> inputNeurons2Weights = getNn().getInputSynapses().get(this);

		for (Neuron input : inputNeurons2Weights.keySet())
			setOutput(getOutput() + input.computeOutput() * inputNeurons2Weights.get(input));

		setOutput(restrictValue(getOutput() + getBias()));

		return getOutput();
	}
}
