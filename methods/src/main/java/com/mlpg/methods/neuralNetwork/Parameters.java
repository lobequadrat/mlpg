package com.mlpg.methods.neuralNetwork;

import lombok.Getter;
import lombok.Setter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Parameters concerning the whole execution can be altered here.
 */
public final class Parameters
{
	private Parameters()
	{
	}

	/** TRAININGCYCLES. */
	public static final int TRAININGCYCLES = 200000;

	/** ERROR_MARGIN_NET. */
	public static final double ERROR_MARGIN_NET = 0.5;
	// public static final double ERROR_MARGIN_NODE = deComputeError(ERROR_MARGIN_NET);

	/** PRECISION. */
	public static final double PRECISION = 100;

	/** LAMBDA. */
	public static final double LAMBDA = 0.5;
	//HC_PROBABILITY_NEURON_IS_TRAINED

	/** HILL_CLIMBING_NEURON_TRAINING_PROBABILITY. */
	public static final double HILL_CLIMBING_NEURON_TRAINING_PROBABILITY = 0.1;

	/** SEED. */
	public static final long SEED = new Random().nextLong();  // XYBox7 -2466293993527162636

	/** RANDOM. */
	public static final Random RANDOM = new Random(SEED);

	/** otherSymbols. */
	public static final DecimalFormatSymbols OTHER_SYMBOLS = new DecimalFormatSymbols(Locale.US);

	/** df. */
	@Getter
	@Setter
	private static DecimalFormat df;

	/** dfl. */
	@Getter
	@Setter
	private static DecimalFormat dfl;

	/** LOGGER. */
	public static final Logger LOGGER = Logger.getLogger(Parameters.class.getName());

	/** COMMA_DELIMITER. */
	public static final String COMMA_DELIMITER = ",";

	/** PLOT_INCREMENT. */
	public static final double PLOT_INCREMENT = 0.02;

	/** INPUT_FALSE: encoded {input | output} x {false | true}. */
	public static final double INPUT_FALSE = 0.0;

	/** INPUT_TRUE: encoded {input | output} x {false | true}. */
	public static final double INPUT_TRUE = 1.0;

	/** OUTPUT_FALSE. */
	public static final double OUTPUT_FALSE = 0.0;

	/** OUTPUT_TRUE. */
	public static final double OUTPUT_TRUE = 1.0;

	/** OUTPUT_UNKNOWN. */
	public static final double OUTPUT_UNKNOWN = 0.5;

	/** MAX_TRIES_BRUTEFORCE. */
	public static final int MAX_TRIES_BRUTEFORCE = 100;

	/** MAX_LAYERS_BRUTEFORCE. */
	public static final int MAX_LAYERS_BRUTEFORCE = 10;

	/** MAX_NEURONS_BRUTEFORCE. */
	public static final int MAX_NEURONS_BRUTEFORCE = 15;
}
