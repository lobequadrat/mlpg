package com.mlpg.methods.neuralNetwork.deeplearning;

/**
 * Activation Function with the RELU method.
 */
public class RELU extends ActivationFunction
{
	@Override
	public double compute(double value)
	{
		return value > 0 ? value : 0;
	}

	@Override
	protected double computeDerivationOfNode(double value)
	{
		return value > 0 ? 1 : 0;
	}
}
