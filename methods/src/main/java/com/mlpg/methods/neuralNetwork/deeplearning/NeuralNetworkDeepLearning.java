package com.mlpg.methods.neuralNetwork.deeplearning;

import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.Parameters;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static com.mlpg.methods.neuralNetwork.Parameters.*;

/**
 * Neural Network implementation with Deep Learning.
 */
public class NeuralNetworkDeepLearning extends NeuralNetwork
{

	private static final double LITTLE_LARGER_ERROR_MARGIN = 1.1;

	@Getter
	private final ActivationFunction defaultActivationFunction;

	private Double lambda;

	public NeuralNetworkDeepLearning(ActivationFunction defaultActivationFunction)
	{
		this.defaultActivationFunction = defaultActivationFunction;
		lambda = LAMBDA;
	}

	public NeuralNetworkDeepLearning(ActivationFunction defaultActivationFunction, double lambda)
	{
		this.defaultActivationFunction = defaultActivationFunction;
		this.lambda = lambda;
	}

	/**
	 * Trains neural network by backpropagation using the train and activation function.
	 * Prepares layers of the neural network for calculating the new weights.
	 * @return totalErrorLastLayer with no changes.
	 */
	@Override
	public double train(double totalErrorLastLayer)
	{
		// last layer
		List<Neuron> neuronsToProcess = getAllNeuronLayers().get(getAllNeuronLayers().size() - 1);
		boolean isOutputLayer = true;

		// process layers successively
		do
		{
			HashSet<Neuron> neuronsToProcessNext = new HashSet<>();

			for (Neuron neuron : neuronsToProcess)
			{
				Map<Neuron, Double> inputNeurons = getInputSynapses().get(neuron);
				if (inputNeurons == null)
					continue;

				for (Neuron inputNeuron : inputNeurons.keySet())
				{
					// reset values to train of input layer neurons
					inputNeuron.setOutputToTrain(0.0);

					// next neuron layer to process
					if (inputNeuron instanceof HiddenNeuronDeepLearning)
						neuronsToProcessNext.add(inputNeuron);
				}
			}

			// compute delta weights and errors of previous layer
			for (Neuron neuron : neuronsToProcess)
				train(neuron.getOutputToTrain(), neuron, isOutputLayer);

			isOutputLayer = false;
			neuronsToProcess = new ArrayList<>(neuronsToProcessNext);
		} while (neuronsToProcess.size() > 0);

		return totalErrorLastLayer;
	}

	/**
	 * Sets new weights for hidden and output layer in the neural network with
	 * activation function. Calculates partial error of each neuron.
	 *
	 * @param partialLoss  0.0 or partial Error of hidden neuron from previous calculations.
	 * @param neuron        Neurons whose connected weights are to be changed.
	 * @param isOutputLayer
	 */
	private void train(double partialLoss, Neuron neuron, boolean isOutputLayer)
	{
		double out = neuron.getOutput();
		Map<Neuron, Double> inputNeurons = getInputSynapses().get(neuron);
		if (inputNeurons == null)
			return;

		ActivationFunction activationFunction;
		if (neuron instanceof NeuronDeepLearning && ((NeuronDeepLearning) neuron).getActivationFunction() != null)
		{
			activationFunction = ((NeuronDeepLearning) neuron).getActivationFunction();
		}
		else
			activationFunction = defaultActivationFunction;

		for (Neuron inputNeuron : inputNeurons.keySet())
		{
			double weight = inputNeurons.get(inputNeuron);

			/*
				can't modify partialError (directly) because it may be used for a set of input neurons
				(partialError - out) is the first delta component (= target - out) of an output neuron
				partialError is error sum of a hidden neuron successively propagated during backpropagation
			*/
			double loss = isOutputLayer ? (out - partialLoss) : partialLoss;
			double gradient = activationFunction.computeDerivationOfNode(out);
			double input = inputNeuron.getOutput();

			// delta rule incl. LAMBDA to compute new weight
			double delta = loss * gradient * input;
			weight -= lambda * delta;
			// change trained weight
			addOrChangeSynapse(inputNeuron, neuron, weight);
			// propagate error to input node (in relation to (already trained?) weight) --> building error sum at hidden neuron
			double outputToTrainChange = loss * gradient * weight;
			inputNeuron.setOutputToTrain(inputNeuron.getOutputToTrain() + outputToTrainChange);
		}
	}

	/**
	 * Sums the errors of the neuron for the total net error and append it on a logger.
	 *
	 * @param fineLog
	 *
	 * @return sums of errors
	 */
	public double getNetError(StringBuilder fineLog)
	{
		double error = 0.0;

		for (Neuron outputNeuron : getOutputNeurons())
		{
			error += getError(outputNeuron);
		}

		if (fineLog != null)
			fineLog.append("\t".concat(this.toString(true).concat(", Error: ").concat(getDfl().format(error)) + "\n"));
		return error;
	}

	/**
	 * Get the error of the output neuron. If the neuron is not null it will allow a little larger
	 * error margin for it to get trained.
	 */
	@Override
	public double getError(Neuron outputNeuron)
	{
		if (outputNeuron.getOutputToTrain() == null)
		{
			if (outputNeuron.getFired() != null)    // true or false interval but should be null
			{
				double newTrainingValue = outputNeuron.getFired()
						? outputNeuron.computeOutput()
						- (OUTPUT_TRUE - LITTLE_LARGER_ERROR_MARGIN * getErrorMarginNode())
						: (OUTPUT_TRUE + LITTLE_LARGER_ERROR_MARGIN * getErrorMarginNode())
						- outputNeuron.computeOutput();

				outputNeuron.setOutputToTrain(newTrainingValue);
				return newTrainingValue;
			}
			else    // should be null and is null ... stay there
			{
				outputNeuron.setOutputToTrain(outputNeuron.computeOutput());
				return 0.0;
			}
		}
		else
		{
			return computeHalfSquaredError(outputNeuron.getOutputToTrain() - outputNeuron.computeOutput());
		}
	}

	public double getErrorMarginNode()
	{
		if (getSizeInputDataSet() == 0)
			Parameters.LOGGER
					.severe("Different Data Input Sets not set! Resulting in infinite small node error margin.");

		// double errorTest = computeHalfSquaredError(nodeError) * getDifferentInputDataSets();
		return Math.sqrt(ERROR_MARGIN_NET / getSizeInputDataSet() * 2 / getOutputNeurons().size());
	}
}
