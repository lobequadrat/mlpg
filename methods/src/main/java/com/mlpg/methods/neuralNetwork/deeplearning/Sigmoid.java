package com.mlpg.methods.neuralNetwork.deeplearning;

/**
 * Activation function with the sigmoid method.
 */
public class Sigmoid extends ActivationFunction
{

	@Override
	public double compute(double value)
	{
		return 1 / (1 + Math.exp(-value));
	}


	@Override
	protected double computeDerivationOfNode(double value)
	{
		return value * (1 - value);
	}

}
