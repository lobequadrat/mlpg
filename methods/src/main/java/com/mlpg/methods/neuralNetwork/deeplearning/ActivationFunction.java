package com.mlpg.methods.neuralNetwork.deeplearning;

/**
 * Abstract class for the backwardpropagation for the neural network.
 */

public abstract class ActivationFunction
{
	/**
	 * Abstract method computing different methods of activation function.
	 *
	 * @param value Output of Neuron
	 */
	protected abstract double compute(double value);

	/**
	 * Abstract method implementation of derivation of neuron for the template method pattern.
	 *
	 * @param value Output of Neuron
	 */
	protected abstract double computeDerivationOfNode(double value);


	/**
	 * Compute delta value for output layer. It contains calculation of deviation from
	 * partialError multiplied with the abstract method and the input related to the weight.
	 *
	 * @param out          Output of Neuron
	 * @param inputWeight  Weight of Neuron
	 * @param partialError
	 * @return the delta value.
	 */
	public double computeDeltaOutputLayer(double partialError, double out, double inputWeight)
	{
		return (out - partialError) * computeDerivationOfNode(out) * inputWeight;
	}

	/**
	 * Compute delta weight for hidden layer neurons with the template method pattern.
	 *
	 * @param out          Output of Neuron
	 * @param inputWeight  Weight of Neuron
	 * @param partialError
	 */
	public double computeDeltaHiddenLayer(double partialError, double out, double inputWeight)
	{
		return partialError * computeDerivationOfNode(out) * inputWeight;
	}
}
