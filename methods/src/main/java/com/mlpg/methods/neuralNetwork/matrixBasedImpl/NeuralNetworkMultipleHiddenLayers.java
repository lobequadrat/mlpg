package com.mlpg.methods.neuralNetwork.matrixBasedImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NeuralNetworkMultipleHiddenLayers
{

    private final double lRate = 0.01;

    private List<Matrix> weights = new ArrayList<>();
    private List<Matrix> bias = new ArrayList<>();

    public NeuralNetworkMultipleHiddenLayers(int... neuronsNo)
    {
        for (int i = 0; i < neuronsNo.length - 1; i++)
        {
            weights.add(new Matrix(neuronsNo[i + 1], neuronsNo[i]));
        }
        for (int i = 0; i < neuronsNo.length - 1; i++)
        {
            bias.add(new Matrix(neuronsNo[i + 1], 1));
        }
    }

    public List<Double> predict(double[] inputSeq)
    {
        Matrix input = Matrix.fromArray(inputSeq);
        List<Matrix> outputs = new ArrayList<>();
        outputs.add(input);
        for (int i = 0; i < weights.size(); i++)
        {
            Matrix output = Matrix.multiply(weights.get(i), outputs.get(i));
            output.add(bias.get(i));
            output.sigmoid();
            outputs.add(output);
        }

        return outputs.get(outputs.size() - 1).toArray();
    }


    public void fit(double[][] sampleIn, double[][] sampleOut, int epochs)
    {
        Random rand = new Random(1);
        for (int i = 0; i < epochs; i++)
        {
            int sampleN = (int) (rand.nextDouble() * sampleIn.length);
            this.train(sampleIn[sampleN], sampleOut[sampleN]);
        }
    }

    public void train(double[] sampleIn, double[] sampleOut)
    {
        // forwardprogagation
        Matrix input = Matrix.fromArray(sampleIn);
        List<Matrix> outputs = new ArrayList<>();
        outputs.add(input);
        for (int i = 0; i < weights.size(); i++)
        {
            Matrix output = Matrix.multiply(weights.get(i), outputs.get(i));
            output.add(bias.get(i));
            output.sigmoid();
            outputs.add(output);
        }

        // backpropagation
        Matrix target = Matrix.fromArray(sampleOut);

        Matrix error = Matrix.subtract(target, outputs.get(outputs.size() - 1));

        for (int i = outputs.size() - 1; i > 0; i--)
        {

            if (i != outputs.size() - 1)
            {
                Matrix wT = Matrix.transpose(weights.get(i));
                error = Matrix.multiply(wT, error);
            }


            Matrix gradient = outputs.get(i).dsigmoid();
            gradient.multiply(error);
            gradient.multiply(lRate);

            Matrix outputT = Matrix.transpose(outputs.get(i - 1));
            Matrix wDelta = Matrix.multiply(gradient, outputT);

            weights.get(i - 1).add(wDelta);
            bias.get(i - 1).add(gradient);
        }

    }
}
