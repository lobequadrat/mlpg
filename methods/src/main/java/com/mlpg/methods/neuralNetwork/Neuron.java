package com.mlpg.methods.neuralNetwork;

import lombok.Getter;
import lombok.Setter;

import java.util.*;


/**
 * Represents a neuron. Abstract class implemented by {@link InputNeuron}
 * and {@link com.mlpg.methods.neuralNetwork.hillClimbing.HiddenNeuronHillClimbing}
 * Neuron stores information whether or not it fired.
 */
@Getter
public abstract class Neuron implements Comparable<Neuron>
{
	@Getter
	private transient NeuralNetwork nn;

	private String id;

	@Getter
	@Setter
	private Double output;

	@Setter
	private Boolean fired = false;

	/**
	 * Simulation run uses bias neurons as a normal input.
	 * Implemented here for quicker referencing.
	 */
	@Setter
	private BiasNeuron biasNeuron;

	@Setter
	private Double outputToTrain;


	// if bias is null = no bias is introduced
	public Neuron(NeuralNetwork nn, String id)
	{
		this.nn = nn;
		this.id = id;
	}

	public void setNetwork(NeuralNetwork nnContainingThisNeuron)
	{
		nn = nnContainingThisNeuron;
	}

	public void connectInput(Neuron inputNeuron)
	{
		connectInput(inputNeuron, (Parameters.RANDOM.nextBoolean() ? -1 : 1) * Parameters.RANDOM.nextDouble());
	}

	public void connectInput(Neuron inputNeuron, double weight)
	{
		connectAndOrChangeWeight(inputNeuron, weight);
	}

	public void connectAndOrChangeWeight(Neuron inputNeuron, Double weight)
	{
		getNn().addOrChangeSynapse(inputNeuron, this, restrictValue(weight));
	}

	/**
	 * Restrict values to [-1,1].
	 * Useful to restrict randomness in randomly changing perceptrons.
	 *
	 * @param value
	 */
	protected double restrictValue(double value)
	{
		return Math.min(Parameters.OUTPUT_TRUE, Math.max(Parameters.OUTPUT_FALSE, value));
	}

	public double getBias()
	{
		return getNn().getInputSynapses().get(this).get(biasNeuron);
	}

	public abstract void train();

	public abstract void undoTraining();

	public void forwardPropagation()
	{
		output = computeOutput();
		fired = output > (Parameters.OUTPUT_TRUE - Parameters.OUTPUT_FALSE) / 2;
	}

	public abstract Double computeOutput();

	@Override
	public String toString()
	{
		return toString(false);
	}

	/**
	 * If firingDetails is true, firing details are logged. Otherwise details about topology are logged
	 *
	 * @param firingDetails
	 * @return
	 */
	public String toString(boolean firingDetails)
	{
		StringBuilder result = new StringBuilder();
		result.append(id.concat("="));

		if (firingDetails)
		{
			result.append(getFired() == null ? "U" : (getFired() ? "F" : "!F"));
			result.append(":".concat(output != null ? Parameters.getDf().format(output) : "null"));
		}
		else
		{
			Map<Neuron, Double> inputNeurons2Weights = getNn().getInputSynapses().get(this);
			if (inputNeurons2Weights != null && inputNeurons2Weights.size() > 0)
			{
				result.append(" [");

				Iterator<Neuron> inputIterator = inputNeurons2Weights.keySet().iterator();
				while (inputIterator.hasNext())
				{
					Neuron input = inputIterator.next();
					result.append(input.id).append(":").append(Parameters.getDf().format(inputNeurons2Weights.get(input)));

					if (inputIterator.hasNext())
						result.append(" ");
				}

				result.append("]");
			}
		}

		return result.toString();
	}

	public String toMATLAB()
	{
		Map<Neuron, Double> inputNeurons2Weights = getNn().getInputSynapses().get(this);
		StringBuilder result = new StringBuilder();
		int variableSuffix = 1;

		if (inputNeurons2Weights != null && inputNeurons2Weights.size() > 0)
		{
			final String variablePrefix = "I";

			boolean firstVariable = true;

			List<Neuron> keys = new ArrayList<>(inputNeurons2Weights.keySet());
			Collections.sort(keys);

			for (Neuron k : keys)
			{
				if (k instanceof BiasNeuron)
					continue;

				double weight = inputNeurons2Weights.get(k);
				if (!firstVariable)
				{
					if (weight < 0)
					{
						result.append(" - ");
						weight *= -1;
					}
					else
						result.append(" + ");
				}
				else
					firstVariable = false;

				result.append(Parameters.getDf().format(weight).concat(" * ").concat(variablePrefix.concat(
						Integer.toString(variableSuffix))));
				variableSuffix++;
			}
		}
		double bias = getBias();
		return "I" + variableSuffix + " = " + result.toString() + (bias < 0 ? " - " + bias * -1 : " + " + bias + ";");
	}

	@Override
	public int compareTo(Neuron n)
	{
		return id.compareTo(n.getId());
	}
}
