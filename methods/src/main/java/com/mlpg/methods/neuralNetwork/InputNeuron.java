package com.mlpg.methods.neuralNetwork;

/**
 * Class represents input neurons.
 * Neurons are able to fire.
 */
public class InputNeuron extends Neuron
{
	private Double input;


	public InputNeuron(NeuralNetwork nn, String name)
	{
		super(nn, name);
		nn.getInputNeurons().add(this);
	}

	public InputNeuron(NeuralNetwork nn, String name, double input)
	{
		super(nn, name);
		changeInput(input);
		nn.getInputNeurons().add(this);
	}

	public void changeInput(double newInput)
	{
		input = newInput;
		setOutput(newInput);
	}

	public Double computeOutput()
	{
		return input;
	}

	@Override
	public void train()
	{
		// intentionally left blank
	}

	@Override
	public void undoTraining()
	{
		// intentionally left blank
	}

	@Override
	public Boolean getFired()
	{
		return true;
	}

	@Override
	public String toString()
	{
		return toString(false);
	}

	public String toString(boolean firingDetails)
	{
		if (firingDetails)
			return getId() + "=" + this.input;

		return getId();
	}
}
