package com.mlpg.methods.neuralNetwork;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.*;
import java.util.logging.Logger;

import static com.mlpg.methods.neuralNetwork.Parameters.*;

/**
 * Represents a neural network.
 * Abstract class implemented by {@link com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing}.
 */
@Getter
@Setter
public abstract class NeuralNetwork
{
	private static final Logger LOGGER = Logger.getLogger(NeuralNetwork.class.getName());

	private List<Neuron> inputNeurons = new ArrayList<>();
	private List<Neuron> hiddenNeurons = new ArrayList<>();
	private List<Neuron> outputNeurons = new ArrayList<>();

	/** Derived from hidden and output neurons. */
	private List<Neuron> hiddenOutputNeurons = new ArrayList<>();

	/**
	 * Contains all neurons with their inputs and weights.
	 */
	private Map<Neuron, Map<Neuron, Double>> inputSynapses = new LinkedHashMap<>();

	/**
	 * Stores all neurons. The input layer has index 0.
	 */
	private List<List<Neuron>> allNeuronLayers;

	private int sizeInputDataSet;

    /*
    public void setNetwork(@NonNull NeuralNetwork nn) {
        for (Neuron i : inputNeurons)
            i.setNetwork(nn);

        for (Neuron h : hiddenNeurons)
            h.setNetwork(nn);

        for (Neuron o : outputNeurons)
            o.setNetwork(nn);
    }
    */

	/**
	 * Initializes {@link NeuralNetwork#allNeuronLayers}.
	 * The neural network is built back to front.
	 * Neurons get stored in a list with associated inputs and weights.
	 * Called after all neurons are created and connected.
	 */
	public void computeLayers()
	{
		allNeuronLayers = new ArrayList<>();
		allNeuronLayers.add(outputNeurons);
		List<Neuron> neuronsToProcess = new ArrayList<>(outputNeurons);

		while (true)
		{
			Set<Neuron> nextNeuronLayer = new HashSet<>();
			for (Neuron neuron : neuronsToProcess)
			{
				Map<Neuron, Double> inputNeurons2Weight = inputSynapses.get(neuron);

				if (inputNeurons2Weight != null)
					nextNeuronLayer.addAll(inputNeurons2Weight.keySet());
			}
			if (nextNeuronLayer.size() <= 0)
				break;

			neuronsToProcess = new ArrayList<>(nextNeuronLayer);
			allNeuronLayers.add(0, neuronsToProcess);
		}
	}

	/**
	 * Calculates output of the neural network.
	 * @return NeuralNetwork for method chaining
	 */
	public NeuralNetwork forwardPropagation()
	{
		for (List<Neuron> layer : allNeuronLayers)
			for (Neuron neuron : layer)
				neuron.forwardPropagation();

		return this;
	}

	/**
	 * Sets inputs used to train neural network.
	 * Checks if number of inputs are equal to number of input neurons.
	 * @param inputs input of the neural network
	 * @return NeuralNetwork with set inputs
	 */
	public NeuralNetwork setInputs(double... inputs)
	{
		int numberInputs = inputs.length;
		if (numberInputs != inputNeurons.size())
		{
			numberInputs = Math.min(numberInputs, inputNeurons.size());

			LOGGER.severe("Number of input Neurons: " + inputNeurons.size()
					+ " does not match the number of inputs requested to set: " + inputs.length
					+ " setting to min( of both ): " + numberInputs);
		}

		for (int n = 0; n < numberInputs; n++)
			((InputNeuron) inputNeurons.get(n)).changeInput(inputs[n]);

		return this;
	}

	/**
	 * Sets target output for neurons used to train neural network.
	 * @param neuron for which output is to be set
	 * @param output the new output
	 * @return NeuralNetwork with configured target outputs
	 */
	public NeuralNetwork setTargetOutput(Neuron neuron, Double output)
	{
		neuron.setOutputToTrain(output);
		return this;
	}

	public NeuralNetwork setTargetOutputs(Double... outputs)
	{
		for (int n = 0; n < outputs.length; n++)
			outputNeurons.get(n).setOutputToTrain(outputs[n]);

		return this;
	}

	/**
	 * Sums up error from output neurons.
	 * @param fineLog if null is passed nothing is logged
	 * @return sum of errors
	 */
	public double getNetError(StringBuilder fineLog)
	{
		double netError = 0.0;

		for (Neuron outputNeuron : outputNeurons)
			netError += getError(outputNeuron);

		if (fineLog != null)
			fineLog.append("\t" + this.toString(true).concat(", Error: ").concat(getDfl().format(netError)) + "\n");

		return netError;
	}

	public double getError(Neuron outputNeuron)
	{
		// REMARK (or only): * neuron.getOutput() --> right now the error is larger
		return computeHalfSquaredError(Math.abs(outputNeuron.getFired() ? OUTPUT_TRUE : OUTPUT_FALSE) - outputNeuron.getOutputToTrain());
	}

	/**
	 * Leads to allNeuronLayers being invalid.
	 * computeLayers must be run again manually due to performance.
	 *
	 * @param inputNeuron the input neuron
	 * @param outputNeuron the output neuron
	 * @param weight the new weight between the input and output neuron
	 */
	public void addOrChangeSynapse(@NonNull Neuron inputNeuron, @NonNull Neuron outputNeuron, double weight)
	{
		Map<Neuron, Double> inputNeuronsMap = inputSynapses.get(outputNeuron);

		if (inputNeuronsMap == null)
			inputNeuronsMap = new LinkedHashMap<>();

		inputNeuronsMap.put(inputNeuron, weight);
		inputSynapses.put(outputNeuron, inputNeuronsMap);
	}

	public abstract double train(double error);

	protected double computeHalfSquaredError(double error)
	{
		return error * error * 0.5;
	}

	@Override
	public String toString()
	{
		return toString(false);
	}

	public String toString(boolean firingDetails)
	{
		StringBuilder neuronsString = new StringBuilder();

		neuronsString.append("I{ ");
		for (Neuron n : inputNeurons)
			neuronsString.append(n.toString(firingDetails)).append(" ");
		neuronsString.append("},  ");

		if (hiddenNeurons.size() > 0)
		{
			neuronsString.append("H{ ");
			for (Neuron n : hiddenNeurons)
				neuronsString.append(n.toString(firingDetails)).append(" ");
			neuronsString.append("},  ");
		}

		neuronsString.append("O{ ");
		for (Neuron n : outputNeurons)
			neuronsString.append(n.toString(firingDetails)).append(" ");
		neuronsString.append("}");

		return neuronsString.toString();
	}

	public String toMATLAB()
	{
		StringBuilder result = new StringBuilder();

		for (Neuron neuron : hiddenOutputNeurons)
			result.append(neuron.toMATLAB().concat("\n"));

		return result.toString();
	}
}
