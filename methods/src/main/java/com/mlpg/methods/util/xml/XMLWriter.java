package com.mlpg.methods.util.xml;

import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import lombok.NonNull;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.Map;

public final class XMLWriter
{
	private XMLWriter()
	{
	}

	/**
	 * Stores the neural network into a xml-file.
	 *
	 * @param neuralNetwork
	 * @param fileName
	 * @throws ParserConfigurationException
	 * @throws TransformerException
	 * @throws FileNotFoundException
	 */
	public static void saveNetwork(
			@NonNull NeuralNetwork neuralNetwork, @NonNull String fileName)
			throws ParserConfigurationException, TransformerException, FileNotFoundException
	{
		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = documentBuilder.newDocument();

		Element rootElement = document.createElement("neuralNetwork");
		Element element1;

		element1 = document.createElement("lastTraining");
		element1.appendChild(document.createComment("TO BE DONE"));
		rootElement.appendChild(element1);

		Element neuronsElement = document.createElement("neurons");
		rootElement.appendChild(neuronsElement);

		xmlAddNeuronList(document, neuronsElement, "inputNeurons", neuralNetwork.getInputNeurons());
		xmlAddNeuronList(document, neuronsElement, "hiddenNeurons", neuralNetwork.getHiddenNeurons());
		xmlAddNeuronList(document, neuronsElement, "outputNeurons", neuralNetwork.getOutputNeurons());

		Element connectionsElement = document.createElement("connectionsElement");
		for (Map.Entry<Neuron, Map<Neuron, Double>> connection : neuralNetwork.getInputSynapses().entrySet())
		{
			Element connectionElement = document.createElement("connection");
			connectionElement.setAttribute("neuron", "" + connection.getKey().getId());
			connectionsElement.appendChild(connectionElement);

			for (Map.Entry<Neuron, Double> inputNeuron : connection.getValue().entrySet())
			{
				Element inputNeuronElement = document.createElement("inputNeuron");
				inputNeuronElement.setAttribute("neuron", "" + inputNeuron.getKey().getId());
				inputNeuronElement.setAttribute("weight", "" + "" + inputNeuron.getValue());
				connectionElement.appendChild(inputNeuronElement);
			}
		}
		rootElement.appendChild(connectionsElement);

		document.appendChild(rootElement);

		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

		FileOutputStream outputStream = new FileOutputStream(fileName);
		transformer.transform(new DOMSource(document), new StreamResult(outputStream));
	}

	/**
	 * Stores a list of neurons into an xml-file.
	 *
	 * @param document
	 * @param parentElement
	 * @param listName
	 * @param neurons
	 */
	private static void xmlAddNeuronList(@NonNull Document document, @NonNull Element parentElement,
										 @NonNull String listName, @NonNull Collection<Neuron> neurons)
	{
		Element listParentElement = document.createElement(listName);

		for (Neuron neuron : neurons)
		{
			Element neuronElement = document.createElement("neuron");

			neuronElement.setAttribute("id", "" + neuron.getId());
			neuronElement.setAttribute("bias", "" + neuron.getBias());
			neuronElement.setAttribute("output", "" + neuron.computeOutput());
			neuronElement.setAttribute("fired", "" + neuron.getFired());

			listParentElement.appendChild(neuronElement);
		}
		parentElement.appendChild(listParentElement);
	}
}
