package com.mlpg.methods.util;

import com.mlpg.methods.neuralNetwork.Neuron;
import lombok.Getter;

import java.util.ArrayList;

import static com.mlpg.methods.neuralNetwork.Parameters.INPUT_FALSE;
import static com.mlpg.methods.neuralNetwork.Parameters.INPUT_TRUE;


@Getter
public class LabelledPointsAndBoxes
{
	private final ArrayList<LabelledPoint> labelledPoints = new ArrayList<>();

	/**
	 * boxes.
	 */
	@Getter
	private static ArrayList<Box> boxes = new ArrayList<>();

	public LabelledPointsAndBoxes()
	{

	}

	public void addLabelledPoint(LabelledPoint lp)
	{
		labelledPoints.add(lp);
	}

	public LabelledPointsAndBoxes(int number, Neuron n)
	{
		this(number, n, false);
	}


	public LabelledPointsAndBoxes(int number, Neuron n, boolean equidistant)
	{
		if (equidistant)
		{
			double numberOfIncrements = Math.sqrt(number);
			double increment = (Math.abs(INPUT_TRUE - INPUT_FALSE)) / (numberOfIncrements - 1);

			for (double x = INPUT_FALSE; x < INPUT_TRUE; x += increment)
				for (double y = INPUT_FALSE; y < INPUT_TRUE; )
				{
					labelledPoints.add(new LabelledPoint(x, y, n));
					y += increment;
				}
		}
		else
			for (int i = 0; i < number; i++)
				labelledPoints.add(new LabelledPoint(null, null, n));
	}
}
