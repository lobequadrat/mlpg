package com.mlpg.methods.util.xml;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import com.mlpg.methods.neuralNetwork.Parameters;
import com.mlpg.methods.neuralNetwork.hillClimbing.HiddenNeuronHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing;
import com.mlpg.methods.neuralNetwork.hillClimbing.OutputNeuronHillClimbing;
import lombok.NonNull;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mlpg.methods.util.xml.XMLConstants.*;

/**
 * Configures a {@link NeuralNetwork} by importing it from an XML file.
 *
 * @author Daniel Süpke
 * @see XMLWriter
 */
public final class XMLReader
{

	/**
	 * https://gitlab.com/lobequadrat/mlpg/-/merge_requests/10#note_497682071 .
	 */
	private static final int START_INDEX_4006 = 0;

	private XMLReader()
	{
	}

	/**
	 * Loads a neural network from a xml-file returns an instance of NeuralNetwork.
	 *
	 * @param fileName
	 * @return
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 */
	@NonNull
	public static NeuralNetwork loadNetwork(@NonNull String fileName) throws ParserConfigurationException,
			IOException, SAXException
	{
		NeuralNetworkHillClimbing neuralNetwork = new NeuralNetworkHillClimbing();
		Map<String, Neuron> nnNeurons = new HashMap<>();

		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

		Document document = documentBuilder.parse(new FileInputStream(fileName));
		Element rootElement = document.getDocumentElement();
		Element neurons = (Element) rootElement.getElementsByTagName(XML_NEURONS).item(START_INDEX_4006);

		addInputNeurons(neuralNetwork, nnNeurons, neurons);
		addHiddenNeurons(neuralNetwork, nnNeurons, neurons);
		addOutputNeurons(neuralNetwork, nnNeurons, neurons);
		addConnections(nnNeurons, rootElement);

		return neuralNetwork;
	}

	/**
	 * Sets connection for neurons in the neural network.
	 *
	 * @param nnNeurons
	 * @param rootElement
	 */
	private static void addConnections(Map<String, Neuron> nnNeurons, Element rootElement)
	{
		Element connections = (Element) rootElement.getElementsByTagName(XML_CONNECTIONS_ELEMENT).item(0);

		for (Element connection : extractElementIterator(connections))
		{
			Neuron neuron = nnNeurons.get(connection.getAttribute(XML_NEURON));

			for (Element inputConnection : extractElementIterator(connection))
			{
				neuron.connectInput(
						nnNeurons.get(inputConnection.getAttribute(XML_NEURON)),
						Double.parseDouble(inputConnection.getAttribute(XML_WEIGHT)));
			}
		}
	}

	/**
	 * Loads output neurons into the neural network.
	 *
	 * @param neuralNetwork
	 * @param nnNeurons
	 * @param neurons
	 */
	private static void addOutputNeurons(NeuralNetworkHillClimbing neuralNetwork, Map<String, Neuron> nnNeurons, Element neurons)
	{
		Element outputNeurons = (Element) neurons.getElementsByTagName(XML_OUTPUT_NEURONS).item(0);

		for (Element outputNeuron : extractElementIterator(outputNeurons))
		{
			Neuron neuron = new OutputNeuronHillClimbing(
					neuralNetwork, outputNeuron.getAttribute(XML_ID)).bias(Double.parseDouble(outputNeuron.getAttribute(XML_BIAS)));
			nnNeurons.put(neuron.getId(), neuron);
		}
	}

	/**
	 * Loads hidden neurons into the neural network.
	 *
	 * @param neuralNetwork
	 * @param nnNeurons
	 * @param neurons
	 */
	private static void addHiddenNeurons(NeuralNetworkHillClimbing neuralNetwork, Map<String, Neuron> nnNeurons, Element neurons)
	{
		Element hiddenNeurons = (Element) neurons.getElementsByTagName(XML_HIDDEN_NEURONS).item(0);

		for (Element hiddenNeuron : extractElementIterator(hiddenNeurons))
		{
			Neuron neuron = new HiddenNeuronHillClimbing(
					neuralNetwork, hiddenNeuron.getAttribute(XML_ID)).bias(Double.parseDouble(hiddenNeuron.getAttribute(XML_BIAS)));
			nnNeurons.put(neuron.getId(), neuron);
		}
	}

	/**
	 * Loads the input neurons into the neural network.
	 *
	 * @param neuralNetwork
	 * @param nnNeurons
	 * @param neurons
	 */
	private static void addInputNeurons(NeuralNetworkHillClimbing neuralNetwork, Map<String, Neuron> nnNeurons, Element neurons)
	{
		Element inputNeurons = (Element) neurons.getElementsByTagName(XML_INPUT_NEURONS).item(0);

		for (Element inputNeuron : extractElementIterator(inputNeurons))
		{
			Neuron neuron = new InputNeuron(neuralNetwork, inputNeuron.getAttribute(XML_ID), Parameters.INPUT_TRUE);
			nnNeurons.put(neuron.getId(), neuron);
		}
	}

	private static List<Element> extractElementIterator(Element parentElement)
	{
		NodeList childNodes = parentElement.getChildNodes();
		List<Element> elementList = new ArrayList<>();

		for (int i = 0; i < childNodes.getLength(); i++)
		{
			if (childNodes.item(i).getNodeType() == Node.ELEMENT_NODE)
			{
				elementList.add((Element) childNodes.item(i));
			}
		}

		return elementList;
	}
}
