package com.mlpg.methods.util;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Box
{
	private Double x1;
	private Double y1;

	private Double x2;
	private Double y2;

	public Box(double x1, double y1, double x2, double y2)
	{
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
}
