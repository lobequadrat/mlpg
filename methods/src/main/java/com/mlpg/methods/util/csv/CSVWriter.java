package com.mlpg.methods.util.csv;

import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.NeuralNetwork;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

import static com.mlpg.methods.neuralNetwork.Parameters.*;

/**
 * Stores result of neuron network into a csv-file.
 */
public class CSVWriter
{
	private OutputStreamWriter writer;
	private Integer numberAttributes;

	/**
	 * Creates an instance of CSVWriter and takes the filename as an input parameter.
	 *
	 * @param fileName Name of file where the result are stored.
	 */
	public CSVWriter(String fileName)
	{
		try
		{
			writer = new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8);
			numberAttributes = 0;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Writes a Header to a csv-file.
	 *
	 * @param datasetName
	 * @param attributeNames
	 */
	public void writeHeader(String datasetName, String... attributeNames)
	{
		// "header:" name of dataset, name of column 1,2,3, ...
		numberAttributes = attributeNames.length;

		StringBuilder attributeNamesSB = new StringBuilder();
		for (int i = 0; i < numberAttributes; i++)
		{
			attributeNamesSB.append(attributeNames[i]);

			if (i + 1 < numberAttributes)
				attributeNamesSB.append(",");
		}

		try
		{
			writer.write("header,".concat(datasetName).concat("\n").concat(attributeNamesSB.toString()).concat("\n"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void writeLine(double... values)
	{
		StringBuilder valuesSB = new StringBuilder();
		for (int i = 0; i < values.length; i++)
		{
			valuesSB.append(values[i]);
			if (i + 1 < values.length)
				valuesSB.append(", ");
		}

		try
		{
			writer.write(valuesSB.toString().concat("\n"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public int writeData(NeuralNetwork nn, Neuron o1, String label, Boolean wasFired)
	{
		int valuesWritten = 0;

		writeHeader(label, "input1", "input2", "output");
		double input1 = INPUT_FALSE;
		while (input1 <= INPUT_TRUE)
		{
			double input2 = INPUT_FALSE;
			while (input2 <= INPUT_TRUE)
			{
				nn.setInputs(input1, input2);
				nn.forwardPropagation();
				if (//wasFired == null ||						// continuously firing neurons
						o1.getFired() == wasFired)                // discretely firing neurons
				{
					writeLine(input1, input2, o1.computeOutput());
					valuesWritten++;
				}

				input2 = Math.round((input2 + PLOT_INCREMENT) * PRECISION) / PRECISION;
			}

			input1 = Math.round((input1 + PLOT_INCREMENT) * PRECISION) / PRECISION;
		}

		return valuesWritten;
	}

	public void close()
	{
		try
		{
			writer.flush();
			writer.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}

