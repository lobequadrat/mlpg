package com.mlpg.methods.util;

import java.util.ArrayList;

/*
    Introduced to check abstractions (e.g., a boolean number) based on an interpretation of true and false
    based on double intervals
 */

public class OutputCheck<T> extends ArrayList<OutputCheck.TwoValues>
{
	public void addComputedAndToTrain(T value1, T value2)
	{
		add(new TwoValues(value1, value2));
	}

	public int allNonEqualValues()
	{
		int nonEqualValues = 0;
		for (TwoValues tv : this)
			if (!tv.checkIfEqual())
				nonEqualValues++;

		return nonEqualValues;
	}

	protected class TwoValues
	{
		private final T valueComputed;
		private final T valueToBeTrained;

		public TwoValues(T valueComputed, T valueToBeTrained)
		{
			this.valueComputed = valueComputed;
			this.valueToBeTrained = valueToBeTrained;
		}

		boolean checkIfEqual()
		{
			if (valueComputed == valueToBeTrained)
				return true;

			if (valueComputed == null)
				return false;

			return valueComputed.equals(valueToBeTrained);
		}
	}
}
