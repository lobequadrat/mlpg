package com.mlpg.methods.util;

import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.Parameters;
import com.mlpg.methods.neuralNetwork.deeplearning.HiddenNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.util.csv.CSVWriter;

import java.text.DecimalFormat;
import java.util.*;
import java.util.logging.Level;

import static com.mlpg.methods.neuralNetwork.Parameters.*;


public final class ManageNN
{
	private ManageNN()
	{
	}

	public static int countTrainingSetEntries(Set<LabelledPointsAndBoxes> trainingSet)
	{
		int countedEntries = 0;

		for (LabelledPointsAndBoxes labelledPointsAndBoxes : trainingSet)
		{
			countedEntries += labelledPointsAndBoxes.getLabelledPoints().size();
		}

		return countedEntries;
	}

	public static double[] trainNN(NeuralNetworkDeepLearning nn,
								   OutputNeuronDeepLearning outputNeuron,
								   Set<LabelledPointsAndBoxes> trainingSet,
								   String outputCSVFile, boolean printToLog)
	{
		nn.computeLayers();
		nn.setSizeInputDataSet(countTrainingSetEntries(trainingSet));

		int iteration = 0;
		double netError;
		OutputCheck<Boolean> outputCheck;
		LabelledPointsAndBoxes trainingPoints = null;
		double pointsIncorrectlyClassified;

		do
		{
			// StringBuilder fineLog = new StringBuilder();
			outputCheck = new OutputCheck<>();

			netError = 0.0;

			for (LabelledPointsAndBoxes labelledPointsAndBoxes : trainingSet)
			{
				trainingPoints = labelledPointsAndBoxes;

				for (LabelledPoint lp : trainingPoints.getLabelledPoints())
				{
					netError +=
							nn.setInputs(lp.getX(), lp.getY()).forwardPropagation().setTargetOutput(lp.getNeuron(),
									lp.getLabel())
									.getError(lp.getNeuron());

					outputCheck.addComputedAndToTrain(lp.getNeuron().getFired(),
							lp.getLabel() == null ? null : lp.getLabel() == OUTPUT_TRUE);
				}

				if (printToLog)
					LOGGER.info(iteration + ". " + nn.toString().concat(", Error: ")
							.concat(getDfl().format(netError))); // + "\n" + fineLog.toString());

				nn.train(netError);
			}

			iteration++;

			// (Math.abs(netError) >= ERROR_MARGIN_NET ||
			pointsIncorrectlyClassified = outputCheck.allNonEqualValues();
		} while (iteration < TRAININGCYCLES && pointsIncorrectlyClassified > 0);

		if (iteration < TRAININGCYCLES)
		{
			writeResultsToFile(nn, trainingPoints, outputNeuron, outputCSVFile);
			LOGGER.info("MATLAB: \r" + nn.toMATLAB());
		}

		double[] result = new double[2];
		result[0] = pointsIncorrectlyClassified;
		result[1] = netError;
		return result;
	}

	public static List<Integer> determineNumberOfLayersAndNeurons(int layersMax, int neuronsMax, boolean randomly)
	{
		int layersToCreate = randomly ? RANDOM.nextInt(layersMax) : layersMax;
		List<Integer> numberOfNeuronsOfLayerX = new ArrayList<>(layersToCreate);

		for (int l = 0; l < layersToCreate; l++)
		{
			numberOfNeuronsOfLayerX.add(randomly ? RANDOM.nextInt(neuronsMax) : neuronsMax);
		}
		return numberOfNeuronsOfLayerX;
	}

	public static boolean increaseLayersMaxNeuronsMax(int[] layersMaxNeuronsMax)
	{
		if (layersMaxNeuronsMax[1] < MAX_NEURONS_BRUTEFORCE)
		{
			layersMaxNeuronsMax[1]++;
			return false;
		}
		else if (layersMaxNeuronsMax[0] < MAX_LAYERS_BRUTEFORCE)
		{
			layersMaxNeuronsMax[0]++;
			layersMaxNeuronsMax[1] = 1;
			return false;
		}

		return true;
	}

	public static void createAndConnectHiddenNeurons(NeuralNetworkDeepLearning nn,
													 Set<Neuron> inputNeurons,
													 List<Integer> numberHiddenLayersAndNeurons,
													 OutputNeuronDeepLearning outputNeuron)
	{
		Set<Neuron> previousNeuronsToConnect = new HashSet<>(inputNeurons);

		for (int l = 0; l < numberHiddenLayersAndNeurons.size(); l++)
		{
			Integer neurons = numberHiddenLayersAndNeurons.get(l);
			Set<Neuron> neuronsCreated = new HashSet<>();
			for (int n = 0; n < neurons; n++)
			{
				HiddenNeuronDeepLearning hiddenNeuron = new HiddenNeuronDeepLearning(nn, "H" + ":l" + l + ":n" + n);
				neuronsCreated.add(hiddenNeuron);
				connectInputNeurons(previousNeuronsToConnect, hiddenNeuron);
			}
			previousNeuronsToConnect = new HashSet<>(neuronsCreated);
		}

		connectInputNeurons(previousNeuronsToConnect, outputNeuron);
	}

	public static void connectInputNeurons(Set<Neuron> previousNeuronsToConnect, HiddenNeuronDeepLearning hiddenNeuron)
	{
		for (Neuron inputNeuron : previousNeuronsToConnect)
		{
			hiddenNeuron.connectInput(inputNeuron);
		}
	}

	public static void writeResultsToFile(NeuralNetworkDeepLearning nn, LabelledPointsAndBoxes trainingPoints,
										  OutputNeuronDeepLearning outputNeuron, String fileName)
	{
		CSVWriter w = new CSVWriter(fileName);

		// trained nn
		LOGGER.info("NN:False values written: " + w.writeData(nn, outputNeuron, "NNFalse", false));
		LOGGER.info("NN:TRUE values written: " + w.writeData(nn, outputNeuron, "NNTrue", true));
		LOGGER.info("NN:UNKNOWN values written: " + w.writeData(nn, outputNeuron, "NNUnknown", null));

		// sample training
		w.writeHeader("BOXFalse", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
		{
			if (lp.getLabel() != null && lp.getLabel() == OUTPUT_FALSE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_FALSE);
		}

		w.writeHeader("BOXFalse", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
		{
			if (lp.getLabel() == null)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_UNKNOWN);
		}

		// sample training
		w.writeHeader("BOXTrue", "input1", "input2", "output");
		for (LabelledPoint lp : trainingPoints.getLabelledPoints())
		{
			if (lp.getLabel() != null && lp.getLabel() == OUTPUT_TRUE)
				w.writeLine(lp.getX(), lp.getY(), OUTPUT_TRUE);
		}

		w.close();
	}

	public static void setup()
	{
		LOGGER.setLevel(Level.ALL);
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");

		OTHER_SYMBOLS.setDecimalSeparator('.');
		OTHER_SYMBOLS.setGroupingSeparator(',');
		setDf(new DecimalFormat("#.#####", OTHER_SYMBOLS));
		setDfl(new DecimalFormat("#.##########", OTHER_SYMBOLS));
	}

	public static void tearDown()
	{
		LOGGER.info("Seed: " + Parameters.SEED);
	}
}
