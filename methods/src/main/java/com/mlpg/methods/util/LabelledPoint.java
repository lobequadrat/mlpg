package com.mlpg.methods.util;

import com.mlpg.methods.neuralNetwork.Neuron;
import lombok.Getter;

import static com.mlpg.methods.neuralNetwork.Parameters.*;

/**
 * Representation of a labelled point.
 */
@Getter
public class LabelledPoint
{
	private final Double x;
	private final Double y;
	private final Neuron neuron;
	private Double label;

	LabelledPoint(Double x, Double y, Neuron neuron)
	{
		this.neuron = neuron;
		this.x = Math.round((x == null ? generateRandomValueAccordingToParameterIntervall() : x) * PRECISION) / PRECISION;
		this.y = Math.round((y == null ? generateRandomValueAccordingToParameterIntervall() : y) * PRECISION) / PRECISION;

		for (Box b : LabelledPointsAndBoxes.getBoxes())
			label = (this.x >= b.getX1() && this.x <= b.getX2() && this.y >= b.getY1() && this.y <= b.getY2())
					? OUTPUT_TRUE : OUTPUT_FALSE; // inside box?
	}

	public LabelledPoint(double x, double y, Neuron neuron, Double label)
	{
		this.neuron = neuron;
		this.x = x;
		this.y = y;
		this.label = label;
	}

	private double generateRandomValueAccordingToParameterIntervall()
	{
		double value = RANDOM.nextDouble();

		// INPUT_FALSE can be defined as e.g., -1.0
		if (INPUT_FALSE < 0)
			value *= RANDOM.nextBoolean() ? -1 : 1;

		return value;
	}
}
