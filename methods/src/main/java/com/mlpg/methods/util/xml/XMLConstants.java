package com.mlpg.methods.util.xml;

/**
 * Contains constants used in XML Files (tags, attributes etc.).
 */
public final class XMLConstants
{
	private XMLConstants()
	{
	}

	static final String XML_BIAS = "bias";
	static final String XML_CONNECTION = "connection";
	static final String XML_CONNECTIONS_ELEMENT = "connectionsElement";
	static final String XML_FIRED = "fired";
	static final String XML_HIDDEN_NEURONS = "hiddenNeurons";
	static final String XML_ID = "id";
	static final String XML_INPUT_NEURON = "inputNeuron";
	static final String XML_INPUT_NEURONS = "inputNeurons";
	static final String XML_LAST_TRAINING = "lastTraining";
	static final String XML_NEURAL_NETWORK = "neuralNetwork";
	static final String XML_NEURON = "neuron";
	static final String XML_NEURONS = "neurons";
	static final String XML_OUTPUT = "output";
	static final String XML_OUTPUT_NEURONS = "outputNeurons";
	static final String XML_WEIGHT = "weight";

}
