package com.mlpg.methods.util.csv;


import com.mlpg.methods.neuralNetwork.Parameters;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Reads results of a neural network stored in csv-files.
 */
public class CSVReader
{
	private BufferedReader reader;


	public CSVReader(String fileName)
	{
		try
		{
			reader = new BufferedReader(new FileReader(fileName));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}


	public String[] readLine()
	{
		String line = null;
		String[] values = null;

		try
		{
			line = reader.readLine();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		if (line != null)
			values = line.split(Parameters.COMMA_DELIMITER);

		return values;
	}

	public void close()
	{
		try
		{
			reader.close();

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}

