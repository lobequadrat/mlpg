package com.mlpg.methods.util;

import com.mlpg.methods.util.csv.CSVReader;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

import static com.mlpg.methods.neuralNetwork.Parameters.PLOT_INCREMENT;

//refactoring

public class Scatter2D extends Application
{
	private static final int FIRST_ATTRIBUTE_INDEX = 0;
	private static final int SECOND_ATTRIBUTE_INDEX = 1;

	/**
	 * Creates a graph for min and max value pairs and displays it.
	 *
	 * @param stage
	 */
	@Override
	public void start(Stage stage)
	{
		stage.setTitle("Scatter Chart 2d");

		CSVReader reader = new CSVReader("output_BOX.csv");
		String[] values;
		XYChart.Series series = null;
		List<XYChart.Series> allSeries = null;

		double minX = Double.MAX_VALUE;
		double maxX = Double.MIN_VALUE;
		double minY = Double.MAX_VALUE;
		double maxY = Double.MIN_VALUE;

		do
		{
			values = reader.readLine();

			if (values == null)
			{
				allSeries.add(series);
				break;
			}

			if ("header".equals(values[FIRST_ATTRIBUTE_INDEX]))
			{
				// read attribute line ... and ignore it
				reader.readLine();

				if (allSeries == null)
					allSeries = new ArrayList<>();
				else
					allSeries.add(series);

				series = new XYChart.Series();
				series.setName(values[SECOND_ATTRIBUTE_INDEX]);
			}
			else
			{
				double x = Double.parseDouble(values[FIRST_ATTRIBUTE_INDEX]);
				double y = Double.parseDouble(values[SECOND_ATTRIBUTE_INDEX]);

				series.getData().add(new XYChart.Data(x, y));

				minX = Math.min(x, minX);
				maxX = Math.max(x, maxX);
				minY = Math.min(y, minY);
				maxY = Math.max(y, maxY);
			}

		} while (values != null);

		final NumberAxis xAxis = new NumberAxis(minX - PLOT_INCREMENT, maxX + PLOT_INCREMENT, PLOT_INCREMENT);
		final NumberAxis yAxis = new NumberAxis(minY - PLOT_INCREMENT, maxY + PLOT_INCREMENT, PLOT_INCREMENT);
		final ScatterChart<Number, Number> sc = new ScatterChart<>(xAxis, yAxis);
		xAxis.setLabel("X");
		yAxis.setLabel("Y");
		sc.setTitle("NN");

		for (XYChart.Series seriesToAdd : allSeries)
			sc.getData().add(seriesToAdd);

		Scene scene = new Scene(sc, 500, 400);
		scene.getStylesheets().add("chart.css");
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args)
	{
		launch(args);
	}
}
