package com.mlpg.methods.util.csv;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;



class CSVReaderTest
{

    CSVReader reader;

    @BeforeEach
    void setUp()
    {
        URL resource = ClassLoader.getSystemResource("csv-reader-test.csv");
        String configPath = URLDecoder.decode(resource.getFile(), StandardCharsets.UTF_8);
        reader = new CSVReader(configPath);
    }

    @Test
    void readLine()
    {
        String[] expected = {"header", "NNFalse"};
        String[] values = reader.readLine();
        reader.close();

        Assertions.assertArrayEquals(expected, values);
    }
}