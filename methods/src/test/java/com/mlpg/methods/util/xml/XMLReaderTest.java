package com.mlpg.methods.util.xml;

import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

public class XMLReaderTest
{

    @SneakyThrows
    @Test
    public void loadNeuralNetworkTest()
    {
        Assertions.assertThrows(FileNotFoundException.class, () -> XMLReader.loadNetwork("foo"));

        URL url = Thread.currentThread().getContextClassLoader().getResource("nn-test.xml");
        NeuralNetwork neuralNetwork = XMLReader.loadNetwork(URLDecoder.decode(url.getPath(), StandardCharsets.UTF_8));

        assertThat(neuralNetwork.getInputNeurons(), hasSize(2));
        assertThat(neuralNetwork.getHiddenNeurons(), hasSize(2));
        assertThat(neuralNetwork.getOutputNeurons(), hasSize(1));

        assertThat(neuralNetwork.getOutputNeurons().get(0).getId(), is("5"));
    }
}
