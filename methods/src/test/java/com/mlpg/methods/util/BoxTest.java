package com.mlpg.methods.util;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;



class BoxTest
{

    @Test
    public void testConstructor()
    {
        Box box = new Box(0.5, 0, 1.5, 1);
        assertEquals(0.5, box.getX1());
        box.setX1(1.0);
        assertNotEquals(0.5, box.getX1());
        assertEquals(1.5, box.getX2());
        assertEquals(0, box.getY1());
        assertEquals(1, box.getY2());
        assertNotNull(box);
    }
}