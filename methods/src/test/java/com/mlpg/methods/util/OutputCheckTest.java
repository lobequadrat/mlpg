package com.mlpg.methods.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



class OutputCheckTest
{

    OutputCheck<Boolean> outputCheck;

    @BeforeEach
    void setUp()
    {
        outputCheck = new OutputCheck<>();
    }

    @Test
    void addComputedAndToTrain()
    {
        outputCheck.addComputedAndToTrain(Boolean.TRUE, Boolean.TRUE);
        Assertions.assertTrue(outputCheck.size() > 0);
    }

    @Test
    void allNonEqualValues()
    {
        outputCheck.addComputedAndToTrain(Boolean.TRUE, Boolean.TRUE);
        outputCheck.addComputedAndToTrain(Boolean.TRUE, Boolean.FALSE);
        outputCheck.addComputedAndToTrain(Boolean.FALSE, Boolean.TRUE);
        outputCheck.addComputedAndToTrain(Boolean.FALSE, Boolean.FALSE);
        Assertions.assertTrue(outputCheck.allNonEqualValues() == 2);
    }

    @Test
    public void checkIfEqualTest()
    {
        outputCheck.addComputedAndToTrain(Boolean.TRUE, Boolean.TRUE);
        Assertions.assertTrue(outputCheck.get(0).checkIfEqual());
    }
}