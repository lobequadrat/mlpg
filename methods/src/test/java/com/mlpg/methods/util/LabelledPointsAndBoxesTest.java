package com.mlpg.methods.util;

import com.mlpg.methods.neuralNetwork.Neuron;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;



class LabelledPointsAndBoxesTest
{

    @Mock
    Neuron neuron;

    @Mock
    LabelledPoint labelledPoint;

    @InjectMocks
    LabelledPointsAndBoxes labelledPointsAndBoxes;

    private static Neuron createNeuron(String id, double bias, boolean fired, double output)
    {
        Neuron neuron = Mockito.mock(Neuron.class);
        Mockito.when(neuron.getId()).then(ret -> id);
        Mockito.when(neuron.getBias()).then(ret -> bias);
        Mockito.when(neuron.getFired()).then(ret -> fired);
        Mockito.when(neuron.computeOutput()).then(ret -> output);
        return neuron;
    }

    private static LabelledPoint createLabelledPoint(double x1, double x2)
    {
        LabelledPoint labelledPoint = Mockito.mock(LabelledPoint.class);
        Mockito.when(labelledPoint.getX()).then(ret -> x1);
        Mockito.when(labelledPoint.getY()).then(ret -> x2);
        return labelledPoint;
    }

    @BeforeEach
    void setUP()
    {
        neuron = createNeuron("1", 0.31, true, 0.21);
    }

    @Test
    void createLabelledPointsAndBoxesTest()
    {
        labelledPointsAndBoxes = Mockito.spy(new LabelledPointsAndBoxes());
        Assertions.assertNotNull(labelledPointsAndBoxes);
    }

    @Test
    void addLabelledPointTest()
    {
        labelledPointsAndBoxes = Mockito.spy(new LabelledPointsAndBoxes());
        labelledPoint = createLabelledPoint(1.0, 3.0);
        labelledPointsAndBoxes.getLabelledPoints();
        labelledPointsAndBoxes.addLabelledPoint(labelledPoint);
        Assertions.assertEquals(1, labelledPointsAndBoxes.getLabelledPoints().size());
    }

    @Test
    void createLabelledPointsWithoutEquidistantTest()
    {
        labelledPointsAndBoxes = new LabelledPointsAndBoxes(400, neuron);
        Assertions.assertNotNull(labelledPointsAndBoxes);
    }
}