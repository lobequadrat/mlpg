package com.mlpg.methods.util;

import com.mlpg.methods.neuralNetwork.Neuron;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;



class LabelledPointTest
{

    LabelledPoint labelledPoint;

    public static Neuron createNeuron(String id, double bias, boolean fired, double output)
    {
        Neuron neuron = Mockito.mock(Neuron.class);
        Mockito.when(neuron.getId()).then(ret -> id);
        Mockito.when(neuron.getBias()).then(ret -> bias);
        Mockito.when(neuron.getFired()).then(ret -> fired);
        Mockito.when(neuron.computeOutput()).then(ret -> output);
        return neuron;
    }

    public static Box createBox(double x1, double x2, double y1, double y2)
    {
        Box box = Mockito.mock(Box.class);
        Mockito.when(box.getX1()).then(ret -> x1);
        Mockito.when(box.getX2()).then(ret -> x2);
        Mockito.when(box.getY1()).then(ret -> y1);
        Mockito.when(box.getY2()).then(ret -> y2);
        return box;
    }

    @Test
    void createLabelledPointTest()
    {
        Neuron n = createNeuron("0", 0.56, false, 0.34);
        labelledPoint = new LabelledPoint(1.0, 2.0, n, -1.0);

        Assertions.assertEquals(1.0, labelledPoint.getX());
        Assertions.assertEquals(2.0, labelledPoint.getY());
        Assertions.assertEquals(-1.0, labelledPoint.getLabel());
        Assertions.assertEquals(n, labelledPoint.getNeuron());
    }

    @Test
    void createNullLabelledPointTest()
    {
        Neuron n = createNeuron("0", 0.56, false, 0.34);
        labelledPoint = new LabelledPoint(null, null, n);

        Assertions.assertNotNull(labelledPoint.getX());
        Assertions.assertNotNull(labelledPoint.getY());
    }

    @Test
    void createLabelledPointInsideBoxTest()
    {
        Neuron n = createNeuron("0", 0.56, false, 0.34);
        Box box = createBox(0.2, 0.4, 0.2, 0.4);
        LabelledPointsAndBoxes.getBoxes().add(box);

        labelledPoint = new LabelledPoint(0.3, 0.3, n);

        Assertions.assertEquals(1.0, labelledPoint.getLabel());
    }

    @Test
    void createLabelledPointOutsideBoxTest()
    {
        Neuron n = createNeuron("0", 0.56, false, 0.34);
        Box box = createBox(0.2, 0.4, 0.2, 0.4);
        LabelledPointsAndBoxes.getBoxes().add(box);

        labelledPoint = new LabelledPoint(0.1, 0.05, n);
        Assertions.assertEquals(0.0, labelledPoint.getLabel());
    }
}