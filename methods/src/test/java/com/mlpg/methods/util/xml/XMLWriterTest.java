package com.mlpg.methods.util.xml;

import com.mlpg.methods.neuralNetwork.NeuralNetwork;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.hillClimbing.NeuralNetworkHillClimbing;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class XMLWriterTest
{

    public NeuralNetwork network = new NeuralNetworkHillClimbing();

    NeuralNetwork spyNetwork = Mockito.spy(network);

    private static File getFileToCompare()
    {
        URL resource = ClassLoader.getSystemResource("nn-test.xml");
        String configPath = URLDecoder.decode(resource.getFile(), StandardCharsets.UTF_8);
        return new File(configPath);
    }

    private static Neuron createNeuron(String id, double bias, boolean fired, double output)
    {
        Neuron neuron = Mockito.mock(Neuron.class);
        Mockito.when(neuron.getId()).then(ret -> id);
        Mockito.when(neuron.getBias()).then(ret -> bias);
        Mockito.when(neuron.getFired()).then(ret -> fired);
        Mockito.when(neuron.computeOutput()).then(ret -> output);
        return neuron;
    }

    @SneakyThrows
    @Test
    public void saveNetworkTest()
    {
        setUpNeuralNetwork();
        XMLWriter.saveNetwork(spyNetwork, "nn-test-compare.xml");

        File testFile = new File("nn-test-compare.xml");
        testFile.deleteOnExit();


        File compareToFile = getFileToCompare();

        assertTrue(FileUtils.contentEquals(testFile, compareToFile), "Generated XML file should be identical to pre generated test XML file");
    }

    public void setUpNeuralNetwork()
    {


        Neuron inputNeuron1 = createNeuron("1", 0.0, true, 1.0);
        Neuron inputNeuron2 = createNeuron("2", 0.0, true, 1.0);
        Neuron hiddenNeuron1 = createNeuron("3", -0.1234, true, 0.5);
        Neuron hiddenNeuron2 = createNeuron("4", 0.1234, false, 1.5);
        Neuron outputNeuron = createNeuron("5", 0.0, true, 0.23);

        // TODO: Mock
        spyNetwork.addOrChangeSynapse(inputNeuron1, hiddenNeuron1, -0.11);
        spyNetwork.addOrChangeSynapse(inputNeuron2, hiddenNeuron1, 0.22);
        spyNetwork.addOrChangeSynapse(inputNeuron1, hiddenNeuron2, 0.33);
        spyNetwork.addOrChangeSynapse(hiddenNeuron1, outputNeuron, 0.44);
        spyNetwork.addOrChangeSynapse(hiddenNeuron2, outputNeuron, 0.55);

        spyNetwork.setInputNeurons(Arrays.asList(inputNeuron1, inputNeuron2));
        spyNetwork.setHiddenNeurons(Arrays.asList(hiddenNeuron1, hiddenNeuron2));
        spyNetwork.setOutputNeurons(Collections.singletonList(outputNeuron));

    }

}
