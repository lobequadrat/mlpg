package com.mlpg.methods.util;

import com.mlpg.methods.neuralNetwork.Neuron;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.util.HashSet;
import java.util.Set;



class ManageNNTest
{

    private static Set<LabelledPointsAndBoxes> getTrainingSet(int number)
    {
        Neuron n = createNeuron("0", 0.56, false, 0.34);
        LabelledPointsAndBoxes trainingPoints = new LabelledPointsAndBoxes(number, n, true);

        Set<LabelledPointsAndBoxes> trainingSet = new HashSet<>();
        trainingSet.add(trainingPoints);
        return trainingSet;
    }

    private static Neuron createNeuron(String id, double bias, boolean fired, double output)
    {
        Neuron neuron = Mockito.mock(Neuron.class);
        Mockito.when(neuron.getId()).then(ret -> id);
        Mockito.when(neuron.getBias()).then(ret -> bias);
        Mockito.when(neuron.getFired()).then(ret -> fired);
        Mockito.when(neuron.computeOutput()).then(ret -> output);
        return neuron;
    }

    @Test
    void countTrainingSetEntries()
    {
        int expected = 400;
        int stored = ManageNN.countTrainingSetEntries(getTrainingSet(expected));
        Assertions.assertTrue(stored == expected);
    }
}