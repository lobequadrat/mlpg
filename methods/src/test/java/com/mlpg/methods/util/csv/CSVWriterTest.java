package com.mlpg.methods.util.csv;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;



class CSVWriterTest
{

    final String fileName = "csv-writer-test.csv";
    CSVWriter writer;

    @BeforeEach
    void setUp()
    {
        writer = new CSVWriter(fileName);
    }

    @Test
    void writeHeader()
    {
        writer.writeHeader("Test", "input1", "input2", "output");
        writer.close();

        boolean found;
        try
        {

            found = FileUtils.readFileToString(new File(fileName), StandardCharsets.UTF_8)
                    .contains("header,Test\ninput1,input2,output");
            Assertions.assertTrue(found);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    void writeLine()
    {
        writer.writeLine(3.21, 4);
        writer.close();
        try
        {
            boolean found = FileUtils.readFileToString(new File(fileName), StandardCharsets.UTF_8)
                    .contains("3.21, 4");
            Assertions.assertTrue(found);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Assertions.fail();
        }
    }
}