package com.mlpg.methods.neuralNetwork;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doCallRealMethod;

public class NeuronTest {

	public NeuralNetwork nn;
	public Neuron neuron;

	public double valueToRestrict = -8.0;


	public void setupRestrictValue()
	{
		neuron = Mockito.mock(Neuron.class);
		Mockito.when(neuron.restrictValue(valueToRestrict)).thenCallRealMethod();
	}

	@Test
	public void restrictValueTest()
	{
		setupRestrictValue();
		double restrictedValue = neuron.restrictValue(valueToRestrict);
		System.out.println("Restrict-Value-Test: Value before: " + valueToRestrict + ", value after: " + restrictedValue);
		assertTrue(restrictedValue >= -1.0 && restrictedValue <= 1.0);
	}

	@Test
	public void setNetworkTest()
	{
		nn = Mockito.mock(NeuralNetwork.class);
		neuron = Mockito.mock(Neuron.class);

		doCallRealMethod().when(neuron).setNetwork(nn);
		doCallRealMethod().when(neuron).getNn();

		neuron.setNetwork(nn);
		assertTrue(nn == neuron.getNn());


	}
}
