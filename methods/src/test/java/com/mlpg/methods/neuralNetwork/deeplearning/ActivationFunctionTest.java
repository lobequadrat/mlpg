package com.mlpg.methods.neuralNetwork.deeplearning;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ActivationFunctionTest
{
	@Test
	public void computeSigmoid()
	{
		Sigmoid activationFunction = new Sigmoid();
		double value = activationFunction.compute(0.5);

		assertEquals(0.6224593312018546, value);
	}

	@Test
	public void computeTanH()
	{
		Tanh activationFunction = new Tanh();
		double value = activationFunction.compute(0.5);

		assertEquals(0.4621171572600098, value);
	}

	@Test
	public void computeRELUBigger0()
	{
		RELU activationFunction = new RELU();
		double value = activationFunction.compute(0.5);

		assertEquals(0.5, value);
	}

	@Test
	public void computeRELULess0()
	{
		RELU activationFunction = new RELU();
		double value = activationFunction.compute(0.5);

		assertEquals(0.5, value);
	}

	@Test
	public void computeDerivationOfNodeSigmoid()
	{
		Sigmoid activationFunction = new Sigmoid();
		double value = activationFunction.computeDerivationOfNode(0.5);

		assertEquals(0.25, value);
	}

	@Test
	public void computeDerivationOfNodeTanH()
	{
		Tanh activationFunction = new Tanh();
		double value = activationFunction.computeDerivationOfNode(0.5);

		assertEquals(0.7864477329659274, value);
	}

	@Test
	public void computeDerivationOfNodeRELUBigger0()
	{
		RELU activationFunction = new RELU();
		double value = activationFunction.computeDerivationOfNode(0.5);

		assertEquals(1, value);
	}

	@Test
	public void computeDerivationOfNodeRELULess0()
	{
		RELU activationFunction = new RELU();
		double value = activationFunction.computeDerivationOfNode(-0.5);

		assertEquals(0, value);
	}

	@Test
	public void computeOutputLayerSigmoid()
	{
		Sigmoid activationFunction = new Sigmoid();
		double value = activationFunction.computeDeltaOutputLayer(0.3, 0.7, 0.8);

		assertEquals(0.06720000000000001, value);
	}

	@Test
	public void computeOutputLayerTanH()
	{
		Tanh activationFunction = new Tanh();
		double value = activationFunction.computeDeltaOutputLayer(0.3, 0.7, 0.8);

		assertEquals(0.20311666879438667, value);
	}

	@Test
	public void computeOutputLayerRELU()
	{
		RELU activationFunction = new RELU();
		double value = activationFunction.computeDeltaOutputLayer(0.3, 0.7, 0.8);

		assertEquals(0.32, value);
	}

	@Test
	public void computeHiddenLayerSigmoid()
	{
		Sigmoid activationFunction = new Sigmoid();
		double value = activationFunction.computeDeltaHiddenLayer(0.3, 0.7, 0.8);

		assertEquals(0.0504, value);
	}

	@Test
	public void computeHiddenLayerTanH()
	{
		Tanh activationFunction = new Tanh();
		double value = activationFunction.computeDeltaHiddenLayer(0.3, 0.7, 0.8);

		assertEquals(0.15233750159579001, value);
	}

	@Test
	public void computeHiddenLayerRELU()
	{
		RELU activationFunction = new RELU();
		double value = activationFunction.computeDeltaHiddenLayer(0.3, 0.7, 0.8);

		assertEquals(0.24, value);
	}

}
