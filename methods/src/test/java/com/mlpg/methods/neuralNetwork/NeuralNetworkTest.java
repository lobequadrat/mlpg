package com.mlpg.methods.neuralNetwork;

import com.mlpg.methods.neuralNetwork.deeplearning.HiddenNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.NeuralNetworkDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.OutputNeuronDeepLearning;
import com.mlpg.methods.neuralNetwork.deeplearning.Sigmoid;
import com.mlpg.methods.util.ManageNN;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import static org.junit.jupiter.api.Assertions.*;


public class NeuralNetworkTest {

	NeuralNetwork nn = new NeuralNetwork() {
		@Override
		public double train(double error)
		{
			return 0;
		}
	};

	public NeuralNetwork nnSpy = Mockito.spy(nn);

	private final NeuralNetworkDeepLearning nnDL = new NeuralNetworkDeepLearning(new Sigmoid());

	private final Neuron i1 = new InputNeuron(nnDL, "I1");
	private final Neuron i2 = new InputNeuron(nnDL, "I2");

	private final Neuron h1 = new HiddenNeuronDeepLearning(nnDL, "H1").bias(0.35);
	private final Neuron h2 = new HiddenNeuronDeepLearning(nnDL, "H2").bias(0.35);

	private final Neuron h3 = new HiddenNeuronDeepLearning(nnDL, "H3").bias(0.35);
	private final Neuron h4 = new HiddenNeuronDeepLearning(nnDL, "H4").bias(0.35);

	private final Neuron o1 = new OutputNeuronDeepLearning(nnDL, "O1").bias(0.6);

	private double error = 0.0;

	public Neuron neuron;
	public Neuron inputNeuron;
	public Neuron outputNeuron;


	public void setupNetwork2HL()
	{
		ManageNN.setup();

		h1.connectInput(i1, 0.15);
		h1.connectInput(i2, 0.2);
		h2.connectInput(i1, 0.25);
		h2.connectInput(i2, 0.3);

		h3.connectInput(h1, 0.25);
		h3.connectInput(h2, 0.3);
		h4.connectInput(h1, 0.25);
		h4.connectInput(h2, 0.3);

		o1.connectInput(h3, 0.4);
		o1.connectInput(h4, 0.45);

		nnDL.computeLayers();

		nnDL.setSizeInputDataSet(1);
		nnDL.setInputs(0.05, 0.1);
	}

	public void setupFiredNull()
	{
		neuron = Mockito.mock(Neuron.class);
		nn = Mockito.mock(NeuralNetwork.class);

		Mockito.when(neuron.getFired()).then(ret -> null);
		Mockito.when(neuron.getOutputToTrain()).then(ret -> 4.0);
		Mockito.when(nn.getError(neuron)).thenCallRealMethod();
		Mockito.when(nn.computeHalfSquaredError(-4.0)).thenCallRealMethod();
	}

	public void setupSynapse()
	{
		inputNeuron = Mockito.mock(Neuron.class);
		outputNeuron = Mockito.mock(Neuron.class);
	}

	/*@Test
	public void getFiredNullTest()
	{
		setupFiredNull();
		assertThat(nn.getError(neuron), is(8.0));
	}*/

	@Test
	public void addSynapseTest()
	{
		setupSynapse();
		nnSpy.addOrChangeSynapse(inputNeuron, outputNeuron, 4.0);
		assertTrue(nnSpy.getInputSynapses().get(outputNeuron).containsValue(4.0));
	}

	@Test
	public void addOrChangeSynapseTest()
	{
		setupSynapse();

		nnSpy.addOrChangeSynapse(inputNeuron, outputNeuron, 4.0);
		assertTrue(nnSpy.getInputSynapses().get(outputNeuron).containsValue(4.0));

		nnSpy.addOrChangeSynapse(inputNeuron, outputNeuron, 5.0);
		assertFalse(nnSpy.getInputSynapses().get(outputNeuron).containsValue(4.0));
		assertTrue(nnSpy.getInputSynapses().get(outputNeuron).containsValue(5.0));
	}

	@Test
	public void correctLayersTest()
	{
		setupNetwork2HL();
		Neuron[] expectedIL = {i1, i2};
		Neuron[] expectedHL1 = {h1, h2};
		Neuron[] expectedHL2 = {h3, h4};
		Neuron[] expectedOL = {o1};

		assertTrue(nnDL.getAllNeuronLayers().size() == 4.0);
		assertThat(nnDL.getAllNeuronLayers().get(0), hasItems(expectedIL));
		assertThat(nnDL.getAllNeuronLayers().get(0), not(hasItems(expectedHL1)));
		assertThat(nnDL.getAllNeuronLayers().get(0), not(hasItems(expectedHL2)));
		assertThat(nnDL.getAllNeuronLayers().get(0), not(hasItems(expectedOL)));

		assertThat(nnDL.getAllNeuronLayers().get(1), hasItems(expectedHL1));
		assertThat(nnDL.getAllNeuronLayers().get(1), not(hasItems(expectedIL)));
		assertThat(nnDL.getAllNeuronLayers().get(1), not(hasItems(expectedHL2)));
		assertThat(nnDL.getAllNeuronLayers().get(1), not(hasItems(expectedOL)));

		assertThat(nnDL.getAllNeuronLayers().get(2), hasItems(expectedHL2));
		assertThat(nnDL.getAllNeuronLayers().get(2), not(hasItems(expectedIL)));
		assertThat(nnDL.getAllNeuronLayers().get(2), not(hasItems(expectedHL1)));
		assertThat(nnDL.getAllNeuronLayers().get(2), not(hasItems(expectedOL)));

		assertThat(nnDL.getAllNeuronLayers().get(3), hasItems(expectedOL));
		assertThat(nnDL.getAllNeuronLayers().get(3), not(hasItems(expectedIL)));
		assertThat(nnDL.getAllNeuronLayers().get(3), not(hasItems(expectedHL1)));
		assertThat(nnDL.getAllNeuronLayers().get(3), not(hasItems(expectedHL2)));
	}

	@Test
	public void setInputsTest()
	{
		setupNetwork2HL();
		assertTrue(i1.getOutput() == 0.05);
		assertTrue(i2.getOutput() == 0.1);
	}

	@Test
	public void netErrorTest()
	{
		setupNetwork2HL();
		nnDL.forwardPropagation();
		double actual = nnDL.getNetError(null);
		assertThat(actual, is(1.33799904094601));
	}

	@Test
	public void halfSquaredTest()
	{
		assertEquals(50.0, nnDL.computeHalfSquaredError(10.0),
				"Half squared not calculated properly");
	}

	@Test
	public void setTargetOutputTest()
	{
		setupNetwork2HL();
		nnDL.setTargetOutput(o1, 1.0);
		assertEquals(1.0, o1.getOutputToTrain());
	}

	@Test
	public void getBiasTest()
	{
		setupNetwork2HL();
		assertEquals(0.35, h1.getBias());
	}

}
