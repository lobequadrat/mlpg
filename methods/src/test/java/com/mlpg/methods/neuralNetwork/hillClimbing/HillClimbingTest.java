package com.mlpg.methods.neuralNetwork.hillClimbing;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.neuralNetwork.Parameters;
import com.mlpg.methods.util.ManageNN;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;

public class HillClimbingTest
{
	private NeuralNetworkHillClimbing nn = new NeuralNetworkHillClimbing();

	NeuralNetworkHillClimbing nnSpy = Mockito.spy(nn);

	private Neuron i1 = new InputNeuron(nnSpy, "I1");
	private Neuron i2 = new InputNeuron(nnSpy, "I2");
	private Neuron i3 = new InputNeuron(nnSpy, "I3");

	private Neuron h1 = new HiddenNeuronHillClimbing(nnSpy, "H1").bias(0.2);
	private Neuron h2 = new HiddenNeuronHillClimbing(nnSpy, "H2").bias(0.2);

	private Neuron h3 = new HiddenNeuronHillClimbing(nnSpy, "H3").bias(0.2);
	private Neuron h4 = new HiddenNeuronHillClimbing(nnSpy, "H4").bias(0.2);

	private OutputNeuronHillClimbing o1 = new OutputNeuronHillClimbing(nnSpy, "O1").bias(0.3);

	private double error;
	private double output;

	public void setupWithHiddenNeurons()
	{
		ManageNN.setup();

		h1.connectInput(i1, 0.3);
		h1.connectInput(i2, 0.4);
		h1.connectInput(i3, -0.7);

		h2.connectInput(i1, -0.8);
		h2.connectInput(i2, -0.5);
		h2.connectInput(i3, 0.7);

		o1.connectInput(h1, 0.4);
		o1.connectInput(h2, 0.6);

		nnSpy.computeLayers();
		nnSpy.setSizeInputDataSet(1);
		error = nnSpy.setInputs(0.0, 0.0, -1.0).setTargetOutputs(0.0).forwardPropagation().getNetError(null);
	}

	public void setupWithoutHiddenNeurons()
	{
		ManageNN.setup();
		o1.connectInput(i1, 0.5);
		o1.connectInput(i2, 0.3);
		o1.connectInput(i3, 0.2);

		nnSpy.computeLayers();
		nnSpy.setSizeInputDataSet(1);
		error = nnSpy.setInputs(0.0, 0.0, -1.0).setTargetOutputs(0.0).forwardPropagation().getNetError(null);
	}

	public void setupWithTwoHiddenLayers()
	{
		ManageNN.setup();
		h1.connectInput(i1, 0.3);
		h1.connectInput(i2, 0.4);
		h1.connectInput(i3, -0.7);

		h2.connectInput(i1, -0.8);
		h2.connectInput(i2, -0.5);
		h2.connectInput(i3, 0.7);

		h3.connectInput(h1, 0.5);
		h3.connectInput(h2, 0.5);

		h4.connectInput(h1, 0.4);
		h4.connectInput(h2, 0.3);

		o1.connectInput(h3, 0.4);
		o1.connectInput(h4, 0.6);

		nnSpy.computeLayers();
		nnSpy.setSizeInputDataSet(1);
		error = nnSpy.setInputs(0.0, 0.0, -1.0).setTargetOutputs(0.0).forwardPropagation().getNetError(null);
	}

	@Test
	public void hillClimbingTestInitial()
	{
		setupWithHiddenNeurons();
		assertEquals(0.0, error);
	}

	@Test
	public void hillClimbingTraining10Test()
	{
		setupWithHiddenNeurons();
		Parameters.RANDOM.setSeed(1);

		for (int i = 0; i < 10; i++)
			error = nnSpy.train(nnSpy.forwardPropagation().getNetError(null));

		assertEquals(0.0, error);
	}

	@Test
	public void comparePreviousErrorElseTest()
	{

		Whitebox.setInternalState(nnSpy, "previousError", 5.0);
		nnSpy.train(6.0);
		Mockito.verify(nnSpy, times(1)).undoTraining();
	}


	@Test
	public void comparePreviousErrorIfTest()
	{
		Whitebox.setInternalState(nnSpy, "previousError", 5.0);
		nnSpy.train(3.0);
		assertEquals(3.0, nnSpy.getPreviousError());
	}


	@Test
	public void comparePreviousErrorNullIfTest()
	{
		Whitebox.setInternalState(nnSpy, "previousError", null);
		nnSpy.train(3.0);
		assertEquals(3.0, nnSpy.getPreviousError());
	}

	private int counter;

	@Test
	public void neuralNetworkHCUndoTraining()
	{

		HiddenNeuronHillClimbing hn1 = Mockito.mock(HiddenNeuronHillClimbing.class);
		Mockito.doAnswer(r -> count()).when(hn1).undoTraining();

		HiddenNeuronHillClimbing hn2 = Mockito.mock(HiddenNeuronHillClimbing.class);
		Mockito.doAnswer(r -> count()).when(hn2).undoTraining();

		HiddenNeuronHillClimbing hn3 = Mockito.mock(HiddenNeuronHillClimbing.class);
		Mockito.doAnswer(r -> count()).when(hn3).undoTraining();

		NeuralNetworkHillClimbing nn = new NeuralNetworkHillClimbing();

		List<Neuron> neuronsTrained = (List<Neuron>) Whitebox.getInternalState(nn, "neuronsTrained");
		neuronsTrained.add(hn1);
		neuronsTrained.add(hn2);
		neuronsTrained.add(hn3);

		counter = 0;
		nn.undoTraining();

		assertEquals(counter, 3);
	}

	public int count()
	{
		return counter++;
	}

	@Test
	public void hillClimbingTestComputeOutputGetFiredFalse()
	{
		setupWithTwoHiddenLayers();
		output = o1.computeOutput();
		assertEquals(0.3, output);
	}



	@Test
	public void hillClimbingTestComputeOutputWithoutHiddenNeurons()
	{
		setupWithoutHiddenNeurons();
		output = o1.computeOutput();
		assertEquals(0.09999999999999998, output);
	}

	@Test
	public void hillClimbingTestComputeOutputWithHiddenNeurons()
	{
		setupWithHiddenNeurons();
		output = o1.computeOutput();
		assertEquals(0.3, output);
	}


	@Test
	public void hiddenNeuronHCTrainTest()
	{
		setupWithHiddenNeurons();
		Parameters.RANDOM.setSeed(1);
		h1.train();
		assertEquals(0.09628009939406179, nnSpy.getInputSynapses().get(h1).get(i1));
		assertEquals(0.5663585279797556, nnSpy.getInputSynapses().get(h1).get(i2));
		assertEquals(0.0, nnSpy.getInputSynapses().get(h1).get(i3));
	}

	@Test
	public void hiddenNeuronHCUndoTraining()
	{
		Parameters.RANDOM.setSeed(1);

		LinkedHashMap<Neuron, Double> oldValue = new LinkedHashMap<>(nnSpy.getInputSynapses().get(h1));
		h1.train();
		LinkedHashMap<Neuron, Double> newValue = new LinkedHashMap<>(nnSpy.getInputSynapses().get(h1));
		h1.undoTraining();
		assertNotEquals(newValue, nnSpy.getInputSynapses().get(h1));
		assertEquals(oldValue, nnSpy.getInputSynapses().get(h1));

	}

	@Test
	public void hillClimbingOutputNeuronConstructorWithoutBiasTest()
	{
		Parameters.RANDOM.setSeed(1);
		OutputNeuronHillClimbing o1 = new OutputNeuronHillClimbing(nnSpy, "O1").bias();
		assertEquals(0.0, o1.getBias());

	}

	@Test
	public void hillClimbingHiddenNeuronConstructorWithoutBiasTest()
	{
		Parameters.RANDOM.setSeed(1);
		HiddenNeuronHillClimbing h1 = new HiddenNeuronHillClimbing(nnSpy, "H1").bias();
		assertEquals(0.0, h1.getBias());
	}
}
