package com.mlpg.methods.neuralNetwork.deeplearning;

import com.mlpg.methods.neuralNetwork.InputNeuron;
import com.mlpg.methods.neuralNetwork.Neuron;
import com.mlpg.methods.util.ManageNN;
import org.junit.jupiter.api.Test;
import org.mockito.internal.util.reflection.Whitebox;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class BackpropagationTest
{
    private final NeuralNetworkDeepLearning nn = new NeuralNetworkDeepLearning(new Sigmoid(), 0.5);

    private final Neuron i1 = new InputNeuron(nn, "I1");
    private final Neuron i2 = new InputNeuron(nn, "I2");

    private final Neuron h1 = new HiddenNeuronDeepLearning(nn, "H1").bias(0.35);
    private final Neuron h11 = new HiddenNeuronDeepLearning(nn, "H11");
    private final Neuron h2 = new HiddenNeuronDeepLearning(nn, "H2").bias(0.35);

    private final Neuron h3 = new HiddenNeuronDeepLearning(nn, "H3").bias(0.35);
    private final Neuron h4 = new HiddenNeuronDeepLearning(nn, "H4").bias(0.35);

    private final OutputNeuronDeepLearning o1 = new OutputNeuronDeepLearning(nn, "O1").bias(0.6);
    private final OutputNeuronDeepLearning o2 = new OutputNeuronDeepLearning(nn, "O2").bias(0.6);
    private final NeuralNetworkDeepLearning nnTanh = new NeuralNetworkDeepLearning(new Tanh());
    private final Neuron i1Tanh = new InputNeuron(nnTanh, "I1");
    private final Neuron i2Tanh = new InputNeuron(nnTanh, "I2");
    private final Neuron h1Tanh = new HiddenNeuronDeepLearning(nnTanh, "H1").bias(0.35);
    private final Neuron h2Tanh = new HiddenNeuronDeepLearning(nnTanh, "H2").bias(0.35);
    private final OutputNeuronDeepLearning o1Tanh = new OutputNeuronDeepLearning(nnTanh, "O1").bias(0.6);
    private final OutputNeuronDeepLearning o2Tanh = new OutputNeuronDeepLearning(nnTanh, "O2").bias(0.6);
    private final NeuralNetworkDeepLearning nnRELU = new NeuralNetworkDeepLearning(new RELU());
    private final Neuron i1RELU = new InputNeuron(nnRELU, "I1");
    private final Neuron i2RELU = new InputNeuron(nnRELU, "I2");
    private final Neuron h1RELU = new HiddenNeuronDeepLearning(nnRELU, "H1").bias(0.35);
    private final Neuron h2RELU = new HiddenNeuronDeepLearning(nnRELU, "H2").bias(0.35);
    private final OutputNeuronDeepLearning o1RELU = new OutputNeuronDeepLearning(nnRELU, "O1").bias(0.6);
    private final OutputNeuronDeepLearning o2RELU = new OutputNeuronDeepLearning(nnRELU, "O2").bias(0.6);
    private double error = 0.0;
    private double errorTanh = 0.0;
    private double errorRELU = 0.0;

    public void setup()
    {
        ManageNN.setup();

        h1.connectInput(i1, 0.15);
        h1.connectInput(i2, 0.2);

        h2.connectInput(i1, 0.25);
        h2.connectInput(i2, 0.3);

        o1.connectInput(h1, 0.4);
        o1.connectInput(h2, 0.45);

        o2.connectInput(h1, 0.5);
        o2.connectInput(h2, 0.55);

        nn.computeLayers();
        nn.setSizeInputDataSet(1);
        error = nn.setInputs(0.05, 0.1).setTargetOutputs(0.01, 0.99).forwardPropagation().getNetError(null);
    }

    public void setupTanh()
    {
        ManageNN.setup();

        h1Tanh.connectInput(i1Tanh, 0.15);
        h1Tanh.connectInput(i2Tanh, 0.2);

        h2Tanh.connectInput(i1Tanh, 0.25);
        h2Tanh.connectInput(i2Tanh, 0.3);

        o1Tanh.connectInput(h1Tanh, 0.4);
        o1Tanh.connectInput(h2Tanh, 0.45);

        o2Tanh.connectInput(h1Tanh, 0.5);
        o2Tanh.connectInput(h2Tanh, 0.55);

        nnTanh.computeLayers();
        nnTanh.setSizeInputDataSet(1);
        errorTanh = nnTanh.setInputs(0.05, 0.1).setTargetOutputs(0.01, 0.99).forwardPropagation().getNetError(null);
    }

    public void setupRELU()
    {
        ManageNN.setup();

        h1RELU.connectInput(i1RELU, 0.15);
        h1RELU.connectInput(i2RELU, 0.2);

        h2RELU.connectInput(i1RELU, 0.25);
        h2RELU.connectInput(i2RELU, 0.3);

        o1RELU.connectInput(h1RELU, 0.4);
        o1RELU.connectInput(h2RELU, 0.45);

        o2RELU.connectInput(h1RELU, 0.5);
        o2RELU.connectInput(h2RELU, 0.55);

        nnRELU.computeLayers();
        nnRELU.setSizeInputDataSet(1);
        errorRELU = nnRELU.setInputs(0.05, 0.1).setTargetOutputs(0.01, 0.99).forwardPropagation().getNetError(null);
    }

    public void setupWithoutHiddenBias()
    {
        ManageNN.setup();

        h11.connectInput(i1, 0.15);
        h11.connectInput(i2, 0.2);

        h2.connectInput(i1, 0.25);
        h2.connectInput(i2, 0.3);

        o1.connectInput(h11, 0.4);
        o1.connectInput(h2, 0.45);

        o2.connectInput(h11, 0.5);
        o2.connectInput(h2, 0.55);

        nn.computeLayers();
        nn.setSizeInputDataSet(1);
        error = nn.setInputs(0.05, 0.1).setTargetOutputs(0.01, 0.99).forwardPropagation().getNetError(null);
    }


    public void setupTwoHiddenLayers()
    {
        ManageNN.setup();

        h1.connectInput(i1, 0.15);
        h1.connectInput(i2, 0.2);

        h2.connectInput(i1, 0.25);
        h2.connectInput(i2, 0.3);

        h3.connectInput(h1, 0.15);
        h3.connectInput(h2, 0.2);

        h4.connectInput(h1, 0.25);
        h4.connectInput(h2, 0.3);

        o1.connectInput(h3, 0.4);
        o1.connectInput(h4, 0.45);

        o2.connectInput(h3, 0.5);
        o2.connectInput(h4, 0.55);

        nn.computeLayers();
        nn.setSizeInputDataSet(1);
        error = nn.setInputs(0.05, 0.1).setTargetOutputs(0.01, 0.99).forwardPropagation().getNetError(null);
    }

    @Test
    public void ForwardpropagationTestInitial()
    {
        setup();
        assertEquals(0.2983711087600027, error, "Initial Network error incorrect.");
    }

    @Test
    public void ForwardpropagationTestInitialTwoHiddenLayers()
    {
        setupTwoHiddenLayers();
        assertEquals(0.3027140236299515, error, "Initial Network error incorrect.");
    }

    @Test
    public void ForwardpropagationTestInitialTanh()
    {
        setupTanh();
        assertEquals(0.28112693346345985, errorTanh, "Initial Network error incorrect.");
    }

    @Test
    public void ForwardpropagationTestInitialRELU()
    {
        setupRELU();
        assertEquals(0.4211247656249999, errorRELU, "Initial Network error incorrect.");
    }

    @Test
    public void ForwardpropagationTestInitialWithoutBias()
    {
        setupWithoutHiddenBias();
        assertTrue(0.2872104151458379 < error && 0.3036452185418678 > error);
    }

    @Test
    public void getErrorFiredTrue()
    {
        setup();
        Whitebox.setInternalState(o1, "fired", true);
        Whitebox.setInternalState(o1, "outputToTrain", null);
        double error = nn.getError(o1);
        assertEquals(0.5291825288575182, error);
    }

    @Test
    public void getErrorFiredFalse()
    {
        setup();
        Whitebox.setInternalState(o1, "fired", false);
        Whitebox.setInternalState(o1, "outputToTrain", null);
        double error = nn.getError(o1);
        assertEquals(1.0264523897528868, error);
    }

    @Test
    public void BackpropagationTestSecondToLastLayer()
    {
        setup();
        nn.train(error);

        assertEquals(0.35891647971788465, nn.getInputSynapses().get(o1).get(h1), "Weight w5 after initial training incorrect.");
        assertEquals(0.4086661860762334, nn.getInputSynapses().get(o1).get(h2), "Weight w6 after initial training incorrect.");
        assertEquals(0.5113012702387375, nn.getInputSynapses().get(o2).get(h1), "Weight w7 after initial training incorrect.");
        assertEquals(0.5613701211079891, nn.getInputSynapses().get(o2).get(h2), "Weight w8 after initial training incorrect.");
    }

    @Test
    public void BackpropagationTestThirdToLastLayer()
    {
        setup();
        nn.train(error);

        assertEquals(0.14981763856120295, nn.getInputSynapses().get(h1).get(i1), "Weight w1 after initial training incorrect.");
        assertEquals(0.19963527712240592, nn.getInputSynapses().get(h1).get(i2), "Weight w2 after initial training incorrect.");
        assertEquals(0.2497881851977662, nn.getInputSynapses().get(h2).get(i1), "Weight w3 after initial training incorrect.");
        assertEquals(0.29957637039553237, nn.getInputSynapses().get(h2).get(i2), "Weight w4 after initial training incorrect.");
    }

    @Test
    public void BackpropagationTestInitial()
    {
        setup();
        nn.train(error);
        error = nn.forwardPropagation().getNetError(null);

        assertEquals(0.2804840766059972, error, "Network error after single training incorrect.");
    }

    @Test
    public void BackpropagationTestInitialTwoHiddenLayers()
    {
        setupTwoHiddenLayers();
        nn.train(error);
        error = nn.forwardPropagation().getNetError(null);

        assertEquals(0.28395693134949646, error, "Network error after single training incorrect.");
    }

    @Test
    public void BackpropagationTestInitialTanh()
    {
        setupTanh();
        nnTanh.train(errorTanh);
        errorTanh = nnTanh.forwardPropagation().getNetError(null);

        assertEquals(0.16608047980773566, errorTanh, "Network error after single training incorrect.");
    }

    @Test
    public void BackpropagationTestInitialRELU()
    {
        setupRELU();
        nnRELU.train(errorRELU);
        errorRELU = nnRELU.forwardPropagation().getNetError(null);

        assertEquals(0.04179246765645772, errorRELU, "Network error after single training incorrect.");
    }

    @Test
    public void BackpropagationTest1000Trainings()
    {
        setup();
        for (int i = 0; i < 1000; i++)
            error = nn.train(nn.forwardPropagation().getNetError(null));

        assertEquals(0.00027018697029697943, error, "Network error after 1000 trainings incorrect.");
    }


    @Test
    public void BackpropagationTest1000TrainingsTanh()
    {
        setupTanh();
        for (int i = 0; i < 1000; i++)
            errorTanh = nnTanh.train(nnTanh.forwardPropagation().getNetError(null));

        assertEquals(3.3682368405947226E-14, errorTanh, "Network error after 1000 trainings incorrect.");
    }

    @Test
    public void BackpropagationTest1000TrainingsRELU()
    {
        setupRELU();
        for (int i = 0; i < 1000; i++)
            errorRELU = nnRELU.train(nnRELU.forwardPropagation().getNetError(null));

        assertEquals(1.504632769052528E-36, errorRELU, "Network error after 1000 trainings incorrect.");
    }

    @Test
    public void BackpropagationTest1000TrainingsTwoHiddenLayers()
    {
        setupTwoHiddenLayers();
        for (int i = 0; i < 1000; i++)
            error = nn.train(nn.forwardPropagation().getNetError(null));

        assertEquals(2.2625506550703423E-4, error, "Network error after 1000 trainings incorrect.");
    }

}